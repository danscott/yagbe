const CART_START: usize = 0x0000;
const VRAM_START: usize = 0x8000;
const SRAM_START: usize = 0xA000;
const WRAM_START: usize = 0xC000;
const ECHO_START: usize = 0xE000;
const OAM_START: usize = 0xFE00;
const IO_PRE: usize = 0xFEA0;
const IO_START: usize = 0xFF00;
const IO_POST: usize = 0xFF4C;
const HRAM_START: usize = 0xFF80;
const IER_START: usize = 0xFFFF;

pub struct Mmu {
	cartridge: [u8; VRAM_START - CART_START],
	vram: [u8; SRAM_START - VRAM_START],
	sram: [u8; WRAM_START - SRAM_START],
	wram: [u8; ECHO_START - WRAM_START],
	oam: [u8; IO_PRE - OAM_START],
	io: [u8; IO_POST - IO_START],
	hram: [u8; IER_START - HRAM_START],
}

macro_rules! read_if_in {
	($address:ident,$start:ident,$end:ident,$s:ident.$mem:ident) => {
		if $start <= ($address as usize) && ($address as usize) < $end {
			return $s.$mem[($address as usize) - $start];
			}
	};
}

macro_rules! write_if_in {
	($address:ident,$val:ident,$start:ident,$end:ident,$s:ident.$mem:ident) => {
		if $start <= ($address as usize) && ($address as usize) < $end {
			$s.$mem[($address as usize) - $start] = $val;
			}
	};
}

impl Mmu {
	pub fn new() -> Mmu {
		Mmu {
			cartridge: [0x00; VRAM_START],
			vram: [0x00; SRAM_START - VRAM_START],
			sram: [0x00; WRAM_START - SRAM_START],
			wram: [0x00; ECHO_START - WRAM_START],
			oam: [0x00; IO_PRE - OAM_START],
			io: [0x00; IO_POST - IO_START],
			hram: [0x00; IER_START - HRAM_START],
		}
	}

	pub fn read_mem_u8(&self, address: u16) -> u8 {
		read_if_in!(address, CART_START, VRAM_START, self.cartridge);

		read_if_in!(address, VRAM_START, SRAM_START, self.vram);

		read_if_in!(address, SRAM_START, WRAM_START, self.sram);

		read_if_in!(address, WRAM_START, ECHO_START, self.wram);

		read_if_in!(address, ECHO_START, OAM_START, self.wram);

		read_if_in!(address, OAM_START, IO_PRE, self.oam);

		read_if_in!(address, IO_START, IO_POST, self.io);

		read_if_in!(address, HRAM_START, IER_START, self.hram);

		0
	}

	pub fn write_mem_u8(&mut self, address: u16, val: u8) {
		write_if_in!(address, val, CART_START, VRAM_START, self.cartridge);

		write_if_in!(address, val, VRAM_START, SRAM_START, self.vram);

		write_if_in!(address, val, SRAM_START, WRAM_START, self.sram);

		write_if_in!(address, val, WRAM_START, ECHO_START, self.wram);

		write_if_in!(address, val, ECHO_START, OAM_START, self.wram);

		write_if_in!(address, val, OAM_START, IO_PRE, self.oam);

		write_if_in!(address, val, IO_START, IO_POST, self.io);

		write_if_in!(address, val, HRAM_START, IER_START, self.hram);
	}

	pub fn read_mem_u16(&self, address: u16) -> u16 {
		u16::from(self.read_mem_u8(address)) | (u16::from(self.read_mem_u8(address + 1)) << 8)
	}

	pub fn write_mem_u16(&mut self, address: u16, val: u16) {
		self.write_mem_u8(address, val as u8);
		self.write_mem_u8(address + 1, (val >> 8) as u8);
	}
}

#[cfg(test)]
mod tests {
	extern crate rand;

	use self::rand::{thread_rng, Rng};
	use super::*;

	#[test]
	fn write_to_cart() {
		let mut mem = Mmu::new();
		let mut rng = thread_rng();
		let address = rng.gen_range(0, VRAM_START) as u16;
		let mem_val: u8 = rng.gen();

		mem.write_mem_u8(address, mem_val);

		assert_eq!(mem.cartridge[address as usize], mem_val);
		assert_eq!(mem_val, mem.read_mem_u8(address));
	}

	#[test]
	fn write_to_vram() {
		let mut mem = Mmu::new();
		let mut rng = thread_rng();
		let address = rng.gen_range(VRAM_START, SRAM_START) as u16;
		let mem_val: u8 = rng.gen();

		mem.write_mem_u8(address, mem_val);

		assert_eq!(mem.vram[address as usize - VRAM_START], mem_val);
		assert_eq!(mem_val, mem.read_mem_u8(address));
	}

	#[test]
	fn write_to_sram() {
		let mut mem = Mmu::new();
		let mut rng = thread_rng();
		let address = rng.gen_range(SRAM_START, WRAM_START) as u16;
		let mem_val: u8 = rng.gen();

		mem.write_mem_u8(address, mem_val);

		assert_eq!(mem.sram[address as usize - SRAM_START], mem_val);
		assert_eq!(mem_val, mem.read_mem_u8(address));
	}

	#[test]
	fn write_to_wram() {
		let mut mem = Mmu::new();
		let mut rng = thread_rng();
		let address = rng.gen_range(WRAM_START, ECHO_START) as u16;
		let mem_val: u8 = rng.gen();

		mem.write_mem_u8(address, mem_val);
		assert_eq!(mem.wram[address as usize - WRAM_START], mem_val);
		assert_eq!(mem_val, mem.read_mem_u8(address));
	}

	#[test]
	fn write_to_echo() {
		let mut mem = Mmu::new();
		let mut rng = thread_rng();
		let address = rng.gen_range(ECHO_START, OAM_START) as u16;
		let mem_val: u8 = rng.gen();

		mem.write_mem_u8(address, mem_val);
		assert_eq!(mem.wram[address as usize - ECHO_START], mem_val);
		assert_eq!(mem_val, mem.read_mem_u8(address));
	}

	#[test]
	fn write_to_oam() {
		let mut mem = Mmu::new();
		let mut rng = thread_rng();
		let address = rng.gen_range(OAM_START, IO_PRE) as u16;
		let mem_val: u8 = rng.gen();

		mem.write_mem_u8(address, mem_val);
		assert_eq!(mem.oam[address as usize - OAM_START], mem_val);
		assert_eq!(mem_val, mem.read_mem_u8(address));
	}

	#[test]
	fn write_to_io() {
		let mut mem = Mmu::new();
		let mut rng = thread_rng();
		let address = rng.gen_range(IO_START, IO_POST) as u16;
		let mem_val: u8 = rng.gen();

		mem.write_mem_u8(address, mem_val);
		assert_eq!(mem.io[address as usize - IO_START], mem_val);
		assert_eq!(mem_val, mem.read_mem_u8(address));
	}

	#[test]
	fn write_to_hram() {
		let mut mem = Mmu::new();
		let mut rng = thread_rng();
		let address = rng.gen_range(HRAM_START, IER_START) as u16;
		let mem_val: u8 = rng.gen();

		mem.write_mem_u8(address, mem_val);
		assert_eq!(mem.hram[address as usize - HRAM_START], mem_val);
		assert_eq!(mem_val, mem.read_mem_u8(address));
	}

}
