pub use cpu::cpu::Cpu;

mod memory;

#[macro_use]
mod registers;

#[macro_use]
mod flags;

mod cpu;

#[cfg(test)]
mod tests {
	extern crate rand;

	use self::rand::{thread_rng, Rng, ThreadRng};
	use super::*;
	use std::collections::HashSet;

	fn context(opcode: u8) -> (Cpu, ThreadRng) {
		let mut cpu = Cpu::new();
		cpu.mmu.write_mem_u8(0x0100, opcode);

		(cpu, thread_rng())
	}

	fn add_u16_u8_as_i8(addr: u16, val: u8) -> u16 {
		((addr as i32) + ((val as i8) as i32)) as u16
	}

	macro_rules! reset_pc {
		($cpu:ident) => {
			set_r16!($cpu, pc, 0x0100);
		};
	}

	#[test]
	fn run_op_0x00() {
		let (mut cpu, _) = context(0x00);

		let cycles = cpu.run_op();

		assert_eq!(4, cycles);

		assert_eq!(0x101, get_r16!(cpu, pc));
	}

	macro_rules! test_ld_r16_d16 {
		(
			$($name:ident,$op:expr,$r16:ident)*
		) => {
			$(
				#[test]
				fn $name() {
					let (mut cpu, mut rng) = context($op);
					let val: u16 = rng.gen();
					cpu.mmu.write_mem_u16(0x0101, val);
					let cycles = cpu.run_op();
					assert_eq!(12, cycles);
					assert_eq!(0x0103, get_r16!(cpu, pc));
					assert_eq!(val, get_r16!(cpu, $r16));
				}
			)*
		};
	}

	test_ld_r16_d16! {
		run_op_0x01, 0x01, bc
		run_op_0x11, 0x11, de
		run_op_0x21, 0x21, hl
		run_op_0x31, 0x31, sp
	}

	macro_rules! test_ld_r16p_r8 {
		(
			$($name:ident,$op:expr,$r16:ident,$r8:ident)*
		) => {
			$(
				#[test]
				fn $name() {
					let (mut cpu, mut rng) = context($op);
					let mut val: u8 = rng.gen();
					let address: u16 = rng.gen_range(0xc000, 0xe000);

					set_r8!(cpu, $r8, val);
					set_r16!(cpu, $r16, address);
					val = get_r8!(cpu, $r8); //for h/l -> hl

					let cycles = cpu.run_op();

					assert_eq!(8, cycles);
					assert_eq!(0x0101, get_r16!(cpu, pc));
					assert_eq!(val, cpu.mmu.read_mem_u8(address));
				}
			)*
		};
	}

	test_ld_r16p_r8! {
		run_op_0x02, 0x02, bc, a
		run_op_0x12, 0x12, de, a
		run_op_0x70, 0x70, hl, b
		run_op_0x71, 0x71, hl, c
		run_op_0x72, 0x72, hl, d
		run_op_0x73, 0x73, hl, e
		run_op_0x74, 0x74, hl, h
		run_op_0x75, 0x75, hl, l
		run_op_0x77, 0x77, hl, a
	}

	macro_rules! test_inc_r16 {
		(
			$($name:ident,$op:expr,$r16:ident)*
		 ) => {
			$(
				#[test]
				fn $name() {
					let (mut cpu, mut rng) = context($op);
					let val: u16 = rng.gen_range(0x0000, 0xfffe);
					set_r16!(cpu, $r16, val);
					let cycles = cpu.run_op();
					assert_eq!(8, cycles);
					assert_eq!(0x0101, get_r16!(cpu, pc));
					assert_eq!(val + 1, get_r16!(cpu, $r16));
				}
			)*
		};
	}

	test_inc_r16! {
		run_op_0x03, 0x03, bc
		run_op_0x13, 0x13, de
		run_op_0x23, 0x23, hl
		run_op_0x33, 0x33, sp
	}

	macro_rules! test_inc_r8 {
		(
			$($name:ident,$op:expr,$r8:ident)*
		) => {
			$(
				#[test]
				fn $name() {
					let (mut cpu, mut rng) = context($op);
					let val: u8 = rng.gen_range(0x00, 0xfe);

					set_r8!(cpu, $r8, val);
					let cycles = cpu.run_op();
					assert_eq!(4, cycles);
					assert_eq!(0x0101, get_r16!(cpu, pc));
					assert_eq!(val + 1, get_r8!(cpu, $r8));
					assert!(is_flag_set!(cpu, n) == false);

					reset_pc!(cpu);
					set_r8!(cpu, $r8, 0x1f);
					cpu.run_op();
					assert!(is_flag_set!(cpu, h));

					reset_pc!(cpu);
					set_r8!(cpu, $r8, 0xff);
					cpu.run_op();
					assert!(is_flag_set!(cpu, z));
				}
			)*
		};
	}

	test_inc_r8! {
		run_op_0x04, 0x04, b
		run_op_0x0c, 0x0c, c
		run_op_0x14, 0x14, d
		run_op_0x1c, 0x1c, e
		run_op_0x24, 0x24, h
		run_op_0x2c, 0x2c, l
		run_op_0x3c, 0x3c, a
	}

	macro_rules! test_dec_r8 {
		(
			$($name:ident,$op:expr,$r8:ident)*
		) => {
			$(
				#[test]
				fn $name() {
					let (mut cpu, mut rng) = context($op);
					let val: u8 = rng.gen_range(0x01, 0xff);

					set_r8!(cpu, $r8, val);
					let cycles = cpu.run_op();
					assert_eq!(4, cycles);
					assert_eq!(0x0101, get_r16!(cpu, pc));
					assert_eq!(val - 1, get_r8!(cpu, $r8));
					assert!(is_flag_set!(cpu, n));

					reset_pc!(cpu);
					set_r8!(cpu, $r8, 0x10);
					cpu.run_op();
					assert!(is_flag_set!(cpu, h));

					reset_pc!(cpu);
					set_r8!(cpu, $r8, 0x01);
					cpu.run_op();
					assert!(is_flag_set!(cpu, z));
				}
			)*
		};
	}

	test_dec_r8! {
		run_op_0x05, 0x05, b
		run_op_0x0d, 0x0d, c
		run_op_0x15, 0x15, d
		run_op_0x1d, 0x1d, e
		run_op_0x25, 0x25, h
		run_op_0x2d, 0x2d, l
		run_op_0x3d, 0x3d, a
	}

	macro_rules! test_ld_r8_d8 {
		(
			$($name:ident,$op:expr,$r8:ident)*
		) => {
			$(
				#[test]
				fn $name() {
					let (mut cpu, mut rng) = context($op);
					let val: u8 = rng.gen();
					cpu.mmu.write_mem_u8(0x101, val);
					let cycles = cpu.run_op();
					assert_eq!(8, cycles);
					assert_eq!(0x102, get_r16!(cpu, pc));
					assert_eq!(val, get_r8!(cpu, $r8));
				}
			)*
		};
	}

	test_ld_r8_d8! {
		run_op_0x06, 0x06, b
		run_op_0x0e, 0x0e, c
		run_op_0x16, 0x16, d
		run_op_0x1e, 0x1e, e
		run_op_0x26, 0x26, h
		run_op_0x2e, 0x2e, l
		run_op_0x3e, 0x3e, a
	}

	#[test]
	fn run_op_0x07() {
		let (mut cpu, mut rng) = context(0x07);
		let val: u8 = rng.gen();

		set_r8!(cpu, a, val);
		set_flag!(cpu, h);
		set_flag!(cpu, n);
		set_flag!(cpu, z);

		let cycles = cpu.run_op();

		assert_eq!(4, cycles);

		assert_eq!(0x101, get_r16!(cpu, pc));
		assert_eq!(val.rotate_left(1), get_r8!(cpu, a));
		assert!(is_flag_set!(cpu, h) == false);
		assert!(is_flag_set!(cpu, n) == false);
		assert!(is_flag_set!(cpu, z) == false);
	}

	#[test]
	fn run_op_0x07_flag_c() {
		let (mut cpu, mut rng) = context(0x07);
		let val: u8 = rng.gen_range(0x80, 0xff);

		set_r8!(cpu, a, val);
		clear_flag!(cpu, c);
		set_flag!(cpu, h);
		set_flag!(cpu, n);
		set_flag!(cpu, z);

		let cycles = cpu.run_op();

		assert_eq!(4, cycles);

		assert_eq!(0x101, get_r16!(cpu, pc));
		assert_eq!(val.rotate_left(1), get_r8!(cpu, a));
		assert!(is_flag_set!(cpu, h) == false);
		assert!(is_flag_set!(cpu, n) == false);
		assert!(is_flag_set!(cpu, z) == false);
		assert!(is_flag_set!(cpu, c));
	}

	#[test]
	fn run_op_0x08() {
		let (mut cpu, mut rng) = context(0x08);
		let val: u16 = rng.gen();
		let mem_loc: u16 = rng.gen_range(0xc000, 0xe000);

		cpu.mmu.write_mem_u16(0x101, mem_loc);
		cpu.registers.sp = val;

		let cycles = cpu.run_op();

		assert_eq!(20, cycles);

		assert_eq!(0x103, get_r16!(cpu, pc));
		assert_eq!(val, cpu.mmu.read_mem_u16(mem_loc));
	}

	macro_rules! test_add_hl_r16 {
		($name:ident,$op:expr,$r16:ident) => {
			#[test]
			fn $name() {
				let (mut cpu, mut rng) = context($op);
				let lhs_val: u16 = rng.gen_range(0x0000, 0x0100);
				let rhs_val: u16 = rng.gen_range(0x0000, 0x0100);

				set_r16!(cpu, hl, lhs_val);
				set_r16!(cpu, $r16, rhs_val);
				set_flag!(cpu, n);
				let cycles = cpu.run_op();
				assert_eq!(8, cycles);
				assert_eq!(0x101, get_r16!(cpu, pc));
				assert_eq!(lhs_val + rhs_val, get_r16!(cpu, hl),);
				assert!(is_flag_set!(cpu, n) == false);

				reset_pc!(cpu);
				set_r16!(cpu, hl, 0xffff);
				set_r16!(cpu, $r16, { rng.gen_range(0x0001, 0xffff) });
				clear_flag!(cpu, c);
				cpu.run_op();
				assert!(is_flag_set!(cpu, c));

				reset_pc!(cpu);
				set_r16!(cpu, hl, 0x00ff);
				set_r16!(cpu, $r16, { rng.gen_range(0x0001, 0xffff) });
				clear_flag!(cpu, h);
				cpu.run_op();
				assert!(is_flag_set!(cpu, h));
			}
		};
		($name:ident,$op:expr) => {
			#[test]
			fn $name() {
				let (mut cpu, mut rng) = context($op);
				let val: u16 = rng.gen_range(0x0000, 0x0100);
				println!("{}", val);
				set_r16!(cpu, hl, val);
				set_flag!(cpu, n);
				let cycles = cpu.run_op();
				assert_eq!(8, cycles);
				assert_eq!(0x101, get_r16!(cpu, pc));
				assert_eq!(val + val, get_r16!(cpu, hl));
				assert!(is_flag_set!(cpu, n) == false);

				reset_pc!(cpu);
				set_r16!(cpu, hl, 0xffff);
				clear_flag!(cpu, c);
				cpu.run_op();
				assert!(is_flag_set!(cpu, c));

				reset_pc!(cpu);
				set_r16!(cpu, hl, 0x00ff);
				clear_flag!(cpu, h);
				cpu.run_op();
				assert!(is_flag_set!(cpu, h));
			}
		};
	}

	test_add_hl_r16!(run_op_0x09, 0x09, bc);
	test_add_hl_r16!(run_op_0x19, 0x19, de);
	test_add_hl_r16!(run_op_0x29, 0x29);
	test_add_hl_r16!(run_op_0x39, 0x39, sp);

	macro_rules! test_ld_r8_r16p {
		($name:ident,$op:expr,$r16p:ident,$r8:ident) => {
			#[test]
			fn $name() {
				let (mut cpu, mut rng) = context($op);
				let val: u8 = rng.gen();
				let address: u16 = rng.gen_range(0xc000, 0xe000);

				cpu.mmu.write_mem_u8(address, val);
				set_r16!(cpu, $r16p, address);

				let cycles = cpu.run_op();

				assert_eq!(8, cycles);

				assert_eq!(0x101, get_r16!(cpu, pc));
				assert_eq!(val, get_r8!(cpu, $r8));
			}
		};
	}

	test_ld_r8_r16p!(run_op_0x0a, 0x0a, bc, a);
	test_ld_r8_r16p!(run_op_0x1a, 0x1a, de, a);
	test_ld_r8_r16p!(run_op_0x46, 0x46, hl, b);
	test_ld_r8_r16p!(run_op_0x4e, 0x4e, hl, c);
	test_ld_r8_r16p!(run_op_0x56, 0x56, hl, d);
	test_ld_r8_r16p!(run_op_0x5e, 0x5e, hl, e);
	test_ld_r8_r16p!(run_op_0x66, 0x66, hl, h);
	test_ld_r8_r16p!(run_op_0x6e, 0x6e, hl, l);
	test_ld_r8_r16p!(run_op_0x7e, 0x7e, hl, a);

	macro_rules! test_dec_r16 {
		($name:ident,$op:expr,$r16:ident) => {
			#[test]
			fn $name() {
				let (mut cpu, mut rng) = context($op);
				let val: u16 = rng.gen();

				set_r16!(cpu, $r16, val);

				let cycles = cpu.run_op();

				assert_eq!(8, cycles);

				assert_eq!(0x0101, get_r16!(cpu, pc));
				assert_eq!(val - 1, get_r16!(cpu, $r16));
			}
		};
	}

	test_dec_r16!(run_op_0x0b, 0x0b, bc);
	test_dec_r16!(run_op_0x1b, 0x1b, de);
	test_dec_r16!(run_op_0x2b, 0x2b, hl);
	test_dec_r16!(run_op_0x3b, 0x3b, sp);

	#[test]
	fn run_op_0x0f() {
		let (mut cpu, mut rng) = context(0x0f);
		let val: u8 = rng.gen();

		set_r8!(cpu, a, val);
		set_flag!(cpu, h);
		set_flag!(cpu, n);
		set_flag!(cpu, z);

		let cycles = cpu.run_op();

		assert_eq!(4, cycles);

		assert_eq!(0x101, get_r16!(cpu, pc));
		assert_eq!(val.rotate_right(1), get_r8!(cpu, a));
		assert!(is_flag_set!(cpu, h) == false);
		assert!(is_flag_set!(cpu, n) == false);
		assert!(is_flag_set!(cpu, z) == false);
	}

	#[test]
	fn run_op_0x0f_flag_c() {
		let (mut cpu, mut rng) = context(0x0f);
		let val: u8 = rng.gen::<u8>() | 0x01;

		set_r8!(cpu, a, val);
		clear_flag!(cpu, c);
		set_flag!(cpu, h);
		set_flag!(cpu, n);
		set_flag!(cpu, z);

		let cycles = cpu.run_op();

		assert_eq!(4, cycles);

		assert_eq!(0x101, get_r16!(cpu, pc));
		assert_eq!(val.rotate_right(1), get_r8!(cpu, a));
		assert!(is_flag_set!(cpu, h) == false);
		assert!(is_flag_set!(cpu, n) == false);
		assert!(is_flag_set!(cpu, z) == false);
		assert!(is_flag_set!(cpu, c));
	}

	#[test]
	fn run_op_0x10() {
		let (mut cpu, _) = context(0x10);

		let cycles = cpu.run_op();

		assert_eq!(4, cycles);

		assert_eq!(0x102, get_r16!(cpu, pc));
		assert!(cpu.stopped);
	}

	#[test]
	fn run_op_0x17() {
		let (mut cpu, mut rng) = context(0x17);
		let val: u8 = rng.gen();

		set_r8!(cpu, a, val);
		clear_flag!(cpu, c);
		set_flag!(cpu, h);
		set_flag!(cpu, n);
		set_flag!(cpu, z);

		let cycles = cpu.run_op();

		assert_eq!(4, cycles);

		assert_eq!(0x0101, get_r16!(cpu, pc));
		assert_eq!(val.wrapping_shl(1), get_r8!(cpu, a));
		assert!(is_flag_set!(cpu, h) == false);
		assert!(is_flag_set!(cpu, n) == false);
		assert!(is_flag_set!(cpu, z) == false);
	}

	#[test]
	fn run_op_0x17_from_c() {
		let (mut cpu, mut rng) = context(0x17);
		let val: u8 = rng.gen();

		set_r8!(cpu, a, val);
		set_flag!(cpu, c);

		cpu.run_op();

		assert_eq!(val.wrapping_shl(1) + 1, get_r8!(cpu, a));
	}

	#[test]
	fn run_op_0x17_flag_c() {
		let (mut cpu, mut rng) = context(0x17);
		let val: u8 = rng.gen_range(0x80, 0xff);

		set_r8!(cpu, a, val);
		clear_flag!(cpu, c);

		cpu.run_op();

		assert!(is_flag_set!(cpu, c));
	}

	#[test]
	fn run_op_0x18_positive() {
		let (mut cpu, mut rng) = context(0x18);
		let val: u8 = rng.gen_range(0x00, 0x80);

		cpu.mmu.write_mem_u8(0x0101, val);

		let cycles = cpu.run_op();

		let expected = add_u16_u8_as_i8(0x0102, val);

		assert_eq!(12, cycles);
		assert_eq!(expected, get_r16!(cpu, pc));
	}

	#[test]
	fn run_op_0x18_negative() {
		let (mut cpu, mut rng) = context(0x18);
		let val: u8 = rng.gen_range(0x80, 0xff);

		cpu.mmu.write_mem_u8(0x0101, val);

		let cycles = cpu.run_op();

		let expected = add_u16_u8_as_i8(0x0102, val);

		assert_eq!(12, cycles);
		assert_eq!(expected, get_r16!(cpu, pc));
	}

	#[test]
	fn run_op_0x1f() {
		let (mut cpu, mut rng) = context(0x1f);
		let val: u8 = rng.gen();

		set_r8!(cpu, a, val);
		set_flag!(cpu, h);
		set_flag!(cpu, n);
		set_flag!(cpu, z);

		let cycles = cpu.run_op();

		assert_eq!(4, cycles);

		assert_eq!(0x101, get_r16!(cpu, pc));
		assert_eq!(val >> 1, get_r8!(cpu, a));
		assert!(is_flag_set!(cpu, h) == false);
		assert!(is_flag_set!(cpu, n) == false);
		assert!(is_flag_set!(cpu, z) == false);
	}

	#[test]
	fn run_op_0x1f_with_carry() {
		let (mut cpu, mut rng) = context(0x1f);
		let val: u8 = rng.gen();

		set_r8!(cpu, a, val);
		set_flag!(cpu, c);
		set_flag!(cpu, h);
		set_flag!(cpu, n);
		set_flag!(cpu, z);

		let cycles = cpu.run_op();

		assert_eq!(4, cycles);

		assert_eq!(0x101, get_r16!(cpu, pc));
		assert_eq!((val >> 1) & 0x80, get_r8!(cpu, a));
		assert!(is_flag_set!(cpu, h) == false);
		assert!(is_flag_set!(cpu, n) == false);
		assert!(is_flag_set!(cpu, z) == false);
	}

	#[test]
	fn run_op_0x1f_flag_c() {
		let (mut cpu, mut rng) = context(0x1f);
		let val: u8 = rng.gen::<u8>() | 0x01;

		set_r8!(cpu, a, val);
		clear_flag!(cpu, c);
		set_flag!(cpu, h);
		set_flag!(cpu, n);
		set_flag!(cpu, z);

		let cycles = cpu.run_op();

		assert_eq!(4, cycles);

		assert_eq!(0x101, get_r16!(cpu, pc));
		assert_eq!(val >> 1, get_r8!(cpu, a));
		assert!(is_flag_set!(cpu, h) == false);
		assert!(is_flag_set!(cpu, n) == false);
		assert!(is_flag_set!(cpu, z) == false);
		assert!(is_flag_set!(cpu, c));
	}

	macro_rules! test_jr_flag_r8 {
		($name:ident,$op:expr,$flag:ident,$check_val:expr) => {
			#[test]
			fn $name() {
				let (mut cpu, mut rng) = context($op);

				let val: u8 = rng.gen();
				cpu.mmu.write_mem_u8(0x0101, val);
				set_flag!(cpu, $flag, { !$check_val });
				let cycles = cpu.run_op();
				assert!(
					8 == cycles,
					"operation should take 8 cycles when {} is {}",
					stringify!($flag),
					!$check_val
				);
				assert!(
					0x0102 == cpu.registers.pc,
					"pc should advance as normal when {} is {}",
					stringify!($flag),
					!$check_val
				);

				reset_pc!(cpu);
				let val = rng.gen_range(0x00, 0x80);
				cpu.mmu.write_mem_u8(0x0101, val);
				set_flag!(cpu, $flag, $check_val);
				let cycles = cpu.run_op();
				assert!(
					12 == cycles,
					"operation should take 12 cycles when {} is {}, but got {}",
					stringify!($flag),
					$check_val,
					cycles
				);
				assert!(
					add_u16_u8_as_i8(0x102, val) == cpu.registers.pc,
					"expected cpu to advance by {} but got {:b}",
					val as i8,
					cpu.registers.pc
				);

				reset_pc!(cpu);
				let val: u8 = rng.gen_range(0x80, 0xff);
				cpu.mmu.write_mem_u8(0x0101, val);
				set_flag!(cpu, $flag, $check_val);
				let cycles = cpu.run_op();
				assert!(
					12 == cycles,
					"operation should take 12 cycles when {} is {}, but got {}",
					stringify!($flag),
					$check_val,
					cycles
				);
				assert!(
					add_u16_u8_as_i8(0x102, val) == cpu.registers.pc,
					"expected cpu to advance by {} but got {:b}",
					val as i8,
					cpu.registers.pc
				);
			}
		};
	}

	test_jr_flag_r8!(run_op_0x20, 0x20, z, false);
	test_jr_flag_r8!(run_op_0x28, 0x28, z, true);
	test_jr_flag_r8!(run_op_0x30, 0x30, c, false);
	test_jr_flag_r8!(run_op_0x38, 0x38, c, true);

	#[test]
	fn run_op_0x22() {
		let (mut cpu, mut rng) = context(0x22);
		let addr: u16 = rng.gen_range(0x8000, 0xc000);
		let val: u8 = rng.gen_range(0x80, 0xff);

		set_r8!(cpu, a, val);
		set_r16!(cpu, hl, addr);

		let cycles = cpu.run_op();

		assert_eq!(8, cycles);
		assert_eq!(addr + 1, get_r16!(cpu, hl));
		assert_eq!(val, cpu.mmu.read_mem_u8(addr));
	}

	macro_rules! test_daa {
	(
		test           n        c        h         bits 3-7                        bits 0-4                         adds         sets c
		$($name:expr; $n:expr; $c:expr; $h:expr; ($tb_start:expr)-($tb_end:expr); ($bb_start:expr)-($bb_end:expr); $added:expr; $c_post:expr;)*
	) => {
		#[test]
		fn run_op_0x27() {
			let (mut cpu, mut rng) = context(0x27);

			$(
				let val = (rng.gen_range::<u8>($tb_start, $tb_end) << 4) | rng.gen_range($bb_start, $bb_end);

				set_r8!(cpu, a, val);
				set_flag!(cpu, c, $c);
				set_flag!(cpu, h, $h);
				set_flag!(cpu, n, $n);
				reset_pc!(cpu);

				let cycles = cpu.run_op();

				assert!(cycles == 4, "should take 4 cycles");
				assert!(cpu.registers.pc == 0x0101, "should only consume 1 op");
				assert!(
					get_r8!(cpu, a) == (val as u16 + $added as u16) as u8,
					"{}: should have added {:b} to {:b} but got {:b}", $name, $added, val, get_r8!(cpu, a)
				);
				assert!(is_flag_set!(cpu, c) == $c_post, "{}: should have set c to {}", $name, $c_post);
			)*
		}
	}
}

	test_daa! {
		test     n        c        h          bits 3-7       bits 0-4     adds    sets c
		"p-1";   false;   false;   false;   (0x0)-(0x9);   (0x0)-(0x9);   0x00;   false;
		"p-2";   false;   false;   false;   (0x0)-(0x8);   (0xa)-(0xf);   0x06;   false;
		"p-3";   false;   false;   true;    (0x0)-(0x9);   (0x0)-(0x3);   0x06;   false;
		"p-4";   false;   false;   false;   (0xa)-(0xf);   (0x0)-(0x9);   0x60;   true;
		"p-5";   false;   false;   false;   (0x9)-(0xf);   (0xa)-(0xf);   0x66;   true;
		"p-6";   false;   false;   true;    (0xa)-(0xf);   (0x0)-(0x3);   0x66;   true;
		"p-7";   false;   true;    false;   (0x0)-(0x2);   (0x0)-(0x9);   0x60;   true;
		"p-8";   false;   true;    false;   (0x0)-(0x2);   (0xa)-(0xf);   0x66;   true;
		"p-9";   false;   true;    true;    (0x0)-(0x3);   (0x0)-(0x3);   0x66;   true;
		"n-1";   true;    false;   false;   (0x0)-(0x9);   (0x0)-(0x9);   0x00;   false;
		"n-2";   true;    false;   true;    (0x0)-(0x8);   (0x6)-(0xf);   0xfa;   false;
		"n-3";   true;    true;    false;   (0x7)-(0xf);   (0x0)-(0x9);   0xa0;   true;
		"n-4";   true;    true;    true;    (0x6)-(0xf);   (0x6)-(0xf);   0x9a;   true;
	}

	#[test]
	fn run_op_0x2a() {
		let (mut cpu, mut rng) = context(0x2a);
		let address: u16 = rng.gen_range(0x8000, 0xc000);
		let val: u8 = rng.gen();

		cpu.mmu.write_mem_u8(address, val);
		set_r16!(cpu, hl, address);

		let cycles = cpu.run_op();

		assert_eq!(8, cycles);
		assert_eq!(0x0101, get_r16!(cpu, pc));
		assert_eq!(val, get_r8!(cpu, a));
		assert_eq!(address + 1, get_r16!(cpu, hl));
	}

	#[test]
	fn run_op_0x2f() {
		let (mut cpu, mut rng) = context(0x2f);
		let val: u8 = rng.gen();

		set_r8!(cpu, a, val);
		clear_flag!(cpu, n);
		clear_flag!(cpu, h);

		let cycles = cpu.run_op();

		assert_eq!(4, cycles);
		assert_eq!(0x0101, get_r16!(cpu, pc));
		assert_eq!(!val, get_r8!(cpu, a));
	}

	#[test]
	fn run_op_0x32() {
		let (mut cpu, mut rng) = context(0x32);
		let addr: u16 = rng.gen_range(0x8000, 0xc000);
		let val: u8 = rng.gen_range(0x80, 0xff);

		set_r8!(cpu, a, val);
		set_r16!(cpu, hl, addr);

		let cycles = cpu.run_op();

		assert_eq!(8, cycles);
		assert_eq!(addr.wrapping_sub(1), get_r16!(cpu, hl));
		assert_eq!(val, cpu.mmu.read_mem_u8(addr));
	}

	#[test]
	fn run_op_0x34() {
		let (mut cpu, mut rng) = context(0x34);
		let addr: u16 = rng.gen_range(0x8000, 0xc000);
		let val: u8 = rng.gen_range(0x80, 0xff);

		cpu.mmu.write_mem_u8(addr, val);
		set_r16!(cpu, hl, addr);
		let cycles = cpu.run_op();
		assert_eq!(12, cycles);
		assert_eq!(0x0101, get_r16!(cpu, pc));
		assert_eq!(val + 1, cpu.mmu.read_mem_u8(addr));
		assert!(is_flag_set!(cpu, n) == false);

		reset_pc!(cpu);
		cpu.mmu.write_mem_u8(addr, 0x1f);
		cpu.run_op();
		assert!(is_flag_set!(cpu, h));

		reset_pc!(cpu);
		cpu.mmu.write_mem_u8(addr, 0xff);
		cpu.run_op();
		assert!(is_flag_set!(cpu, z));
	}

	#[test]
	fn run_op_0x35() {
		let (mut cpu, mut rng) = context(0x35);
		let addr: u16 = rng.gen_range(0x8000, 0xc000);
		let val: u8 = rng.gen_range(0x01, 0xff);

		cpu.mmu.write_mem_u8(addr, val);
		set_r16!(cpu, hl, addr);
		let cycles = cpu.run_op();
		assert_eq!(12, cycles);
		assert_eq!(0x0101, get_r16!(cpu, pc));
		assert_eq!(val - 1, cpu.mmu.read_mem_u8(addr));
		assert!(is_flag_set!(cpu, n));

		reset_pc!(cpu);
		cpu.mmu.write_mem_u8(addr, 0x10);
		cpu.run_op();
		assert!(is_flag_set!(cpu, h));

		reset_pc!(cpu);
		cpu.mmu.write_mem_u8(addr, 0x01);
		cpu.run_op();
		assert!(is_flag_set!(cpu, z));
	}

	#[test]
	fn run_op_0x36() {
		let (mut cpu, mut rng) = context(0x36);
		let addr: u16 = rng.gen_range(0x8000, 0xc000);
		let val: u8 = rng.gen();

		set_r16!(cpu, hl, addr);
		cpu.mmu.write_mem_u8(0x0101, val);

		let cycles = cpu.run_op();

		assert_eq!(12, cycles);
		assert_eq!(0x0102, get_r16!(cpu, pc));
		assert_eq!(val, cpu.mmu.read_mem_u8(addr));
	}

	#[test]
	fn run_op_0x37() {
		let (mut cpu, _) = context(0x37);

		clear_flag!(cpu, c);
		set_flag!(cpu, n);
		set_flag!(cpu, z);

		let cycles = cpu.run_op();

		assert_eq!(4, cycles);
		assert_eq!(0x0101, get_r16!(cpu, pc));
		assert!(is_flag_set!(cpu, c));
		assert!(is_flag_set!(cpu, n) == false);
		assert!(is_flag_set!(cpu, h) == false);
	}

	#[test]
	fn run_op_0x3a() {
		let (mut cpu, mut rng) = context(0x3a);
		let address: u16 = rng.gen_range(0x8000, 0xc000);
		let val: u8 = rng.gen();

		cpu.mmu.write_mem_u8(address, val);
		set_r16!(cpu, hl, address);

		let cycles = cpu.run_op();

		assert_eq!(8, cycles);
		assert_eq!(0x0101, get_r16!(cpu, pc));
		assert_eq!(val, get_r8!(cpu, a));
		assert_eq!(address - 1, get_r16!(cpu, hl));
	}

	#[test]
	fn run_op_0x3f() {
		let (mut cpu, _) = context(0x3f);

		set_flag!(cpu, c);
		set_flag!(cpu, n);
		set_flag!(cpu, h);
		let cycles = cpu.run_op();
		assert_eq!(4, cycles);
		assert!(is_flag_set!(cpu, c) == false);
		assert!(is_flag_set!(cpu, n) == false);
		assert!(is_flag_set!(cpu, h) == false);

		clear_flag!(cpu, c);
		reset_pc!(cpu);
		cpu.run_op();
		assert!(is_flag_set!(cpu, c));
	}

	macro_rules! test_ld_r8_r8 {
	(
		$($name:ident;$op:expr;$to_r8:ident;$from_r8:ident;)*
	) => {
		$(
			#[test]
			fn $name() {
				let (mut cpu, mut rng) = context($op);
				let val: u8 = rng.gen();

				set_r8!(cpu, $from_r8, val);

				let cycles = cpu.run_op();

				assert_eq!(4, cycles);
				assert_eq!(0x0101, get_r16!(cpu, pc));
				assert_eq!(val, get_r8!(cpu, $to_r8));
			}
		)*
	}
}

	test_ld_r8_r8! {
		run_op_0x40;  0x40;  b;  b;
		run_op_0x41;  0x41;  b;  c;
		run_op_0x42;  0x42;  b;  d;
		run_op_0x43;  0x43;  b;  e;
		run_op_0x44;  0x44;  b;  h;
		run_op_0x45;  0x45;  b;  l;
		run_op_0x47;  0x47;  b;  a;
		run_op_0x48;  0x48;  c;  b;
		run_op_0x49;  0x49;  c;  c;
		run_op_0x4a;  0x4a;  c;  d;
		run_op_0x4b;  0x4b;  c;  e;
		run_op_0x4c;  0x4c;  c;  h;
		run_op_0x4d;  0x4d;  c;  l;
		run_op_0x4f;  0x4f;  c;  a;

		run_op_0x50;  0x50;  d;  b;
		run_op_0x51;  0x51;  d;  c;
		run_op_0x52;  0x52;  d;  d;
		run_op_0x53;  0x53;  d;  e;
		run_op_0x54;  0x54;  d;  h;
		run_op_0x55;  0x55;  d;  l;
		run_op_0x57;  0x57;  d;  a;
		run_op_0x58;  0x58;  e;  b;
		run_op_0x59;  0x59;  e;  c;
		run_op_0x5a;  0x5a;  e;  d;
		run_op_0x5b;  0x5b;  e;  e;
		run_op_0x5c;  0x5c;  e;  h;
		run_op_0x5d;  0x5d;  e;  l;
		run_op_0x5f;  0x5f;  e;  a;

		run_op_0x60;  0x60;  h;  b;
		run_op_0x61;  0x61;  h;  c;
		run_op_0x62;  0x62;  h;  d;
		run_op_0x63;  0x63;  h;  e;
		run_op_0x64;  0x64;  h;  h;
		run_op_0x65;  0x65;  h;  l;
		run_op_0x67;  0x67;  h;  a;
		run_op_0x68;  0x68;  l;  b;
		run_op_0x69;  0x69;  l;  c;
		run_op_0x6a;  0x6a;  l;  d;
		run_op_0x6b;  0x6b;  l;  e;
		run_op_0x6c;  0x6c;  l;  h;
		run_op_0x6d;  0x6d;  l;  l;
		run_op_0x6f;  0x6f;  l;  a;

		run_op_0x78;  0x78;  a;  b;
		run_op_0x79;  0x79;  a;  c;
		run_op_0x7a;  0x7a;  a;  d;
		run_op_0x7b;  0x7b;  a;  e;
		run_op_0x7c;  0x7c;  a;  h;
		run_op_0x7d;  0x7d;  a;  l;
		run_op_0x7f;  0x7f;  a;  a;
	}

	#[test]
	fn run_op_0x76() {
		let (mut cpu, _) = context(0x76);

		let cycles = cpu.run_op();

		assert_eq!(1, cycles);
		assert_eq!(0x0101, get_r16!(cpu, pc));
		assert!(cpu.halted);
	}

	macro_rules! test_add_a_r8 {
	(
		$($name:ident;$op:expr;$r8:ident;)*
	) => {
		$(
			#[test]
			fn $name() {
				let (mut cpu, mut rng) = context($op);
				let l_val: u8 = rng.gen();
				let r_val: u8 = rng.gen();

				set_r8!(cpu, a, l_val);
				set_r8!(cpu, $r8, r_val);
				set_flag!(cpu, n);

				let expected = (get_r8!(cpu, a) as u16 + r_val as u16) as u8;

				let cycles = cpu.run_op();
				assert_eq!(4, cycles);
				assert_eq!(0x0101, get_r16!(cpu, pc));
				assert!(is_flag_set!(cpu, n) == false);
				assert!(expected == get_r8!(cpu, a), "Expected {} + {} to be {} but got {}", l_val, r_val, expected, get_r8!(cpu, a));

				set_r8!(cpu, a, 0x0f);
				set_r8!(cpu, $r8, 0x0f);
				reset_pc!(cpu);
				clear_flag!(cpu, h);
				cpu.run_op();
				assert!(is_flag_set!(cpu, h));

				set_r8!(cpu, a, 0xff);
				set_r8!(cpu, $r8, 0xff);
				reset_pc!(cpu);
				clear_flag!(cpu, c);
				cpu.run_op();
				assert!(is_flag_set!(cpu, c));

				set_r8!(cpu, a, 0x01);
				set_r8!(cpu, $r8, 0xff);
				reset_pc!(cpu);
				clear_flag!(cpu, z);
				cpu.run_op();
				assert!(is_flag_set!(cpu, c));
			}
		)*
	};
}

	test_add_a_r8! {
		run_op_0x80;  0x80;  b;
		run_op_0x81;  0x81;  c;
		run_op_0x82;  0x82;  d;
		run_op_0x83;  0x83;  e;
		run_op_0x84;  0x84;  h;
		run_op_0x85;  0x85;  l;
		run_op_0x87;  0x87;  a;
	}

	#[test]
	fn run_op_0x86() {
		let (mut cpu, mut rng) = context(0x86);
		let address: u16 = rng.gen_range(0x8000, 0xc000);
		let l_val: u8 = rng.gen();
		let r_val: u8 = rng.gen();

		set_flag!(cpu, n);
		cpu.mmu.write_mem_u8(address, r_val);
		set_r16!(cpu, hl, address);
		set_r8!(cpu, a, l_val);
		let cycles = cpu.run_op();
		assert_eq!(8, cycles);
		assert_eq!(0x0101, get_r16!(cpu, pc));
		assert!(is_flag_set!(cpu, n) == false);
		assert_eq!((l_val as u16 + r_val as u16) as u8, get_r8!(cpu, a));

		set_r8!(cpu, a, 0x0f);
		cpu.mmu.write_mem_u8(address, 0x0f);
		reset_pc!(cpu);
		clear_flag!(cpu, h);
		cpu.run_op();
		assert!(is_flag_set!(cpu, h));

		set_r8!(cpu, a, 0xff);
		cpu.mmu.write_mem_u8(address, 0xff);
		reset_pc!(cpu);
		clear_flag!(cpu, c);
		cpu.run_op();
		assert!(is_flag_set!(cpu, c));

		set_r8!(cpu, a, 0x01);
		cpu.mmu.write_mem_u8(address, 0xff);
		reset_pc!(cpu);
		clear_flag!(cpu, z);
		cpu.run_op();
		assert!(is_flag_set!(cpu, c));
	}

	macro_rules! test_adc_a_r8 {
	(
		$($name:ident; $op:expr; $r8:ident;)*
	) => {
		$(
			#[test]
			fn $name() {
				let (mut cpu, mut rng) = context($op);
				let l_val: u8 = rng.gen();
				let r_val: u8 = rng.gen();

				set_r8!(cpu, a, l_val);
				set_r8!(cpu, $r8, r_val);
				set_flag!(cpu, n);

				let expected = (get_r8!(cpu, a) as u16 + r_val as u16) as u8;

				let cycles = cpu.run_op();
				assert_eq!(4, cycles);
				assert_eq!(0x0101, get_r16!(cpu, pc));
				assert!(is_flag_set!(cpu, n) == false);
				assert!(expected == get_r8!(cpu, a), "Expected {} + {} to be {} but got {}", l_val, r_val, expected, get_r8!(cpu, a));

				set_r8!(cpu, a, 0x0f);
				set_r8!(cpu, $r8, 0x0f);
				reset_pc!(cpu);
				clear_flag!(cpu, h);
				cpu.run_op();
				assert!(is_flag_set!(cpu, h));

				set_r8!(cpu, a, 0xff);
				set_r8!(cpu, $r8, 0xff);
				reset_pc!(cpu);
				clear_flag!(cpu, c);
				cpu.run_op();
				assert!(is_flag_set!(cpu, c));

				set_r8!(cpu, a, 0x01);
				set_r8!(cpu, $r8, 0xff);
				reset_pc!(cpu);
				clear_flag!(cpu, z);
				cpu.run_op();
				assert!(is_flag_set!(cpu, c));

				set_r8!(cpu, a, 0x01);
				set_r8!(cpu, $r8, 0x01);
				set_flag!(cpu, c);
				reset_pc!(cpu);
				cpu.run_op();
				assert_eq!(0x03, get_r8!(cpu, a));
			}
		)*
	};
}

	test_adc_a_r8! {
		run_op_0x88; 0x88; b;
		run_op_0x89; 0x89; c;
		run_op_0x8a; 0x8a; d;
		run_op_0x8b; 0x8b; e;
		run_op_0x8c; 0x8c; h;
		run_op_0x8d; 0x8d; l;
	}

	#[test]
	fn run_op_0x8e() {
		let (mut cpu, mut rng) = context(0x8e);
		let l_val: u8 = rng.gen();
		let address: u16 = rng.gen_range(0x8000, 0xc000);
		let r_val: u8 = rng.gen();

		set_r16!(cpu, hl, address);
		set_r8!(cpu, a, l_val);
		cpu.mmu.write_mem_u8(address, r_val);
		set_flag!(cpu, n);

		let expected = (get_r8!(cpu, a) as u16 + r_val as u16) as u8;

		let cycles = cpu.run_op();
		assert_eq!(4, cycles);
		assert_eq!(0x0101, get_r16!(cpu, pc));
		assert!(is_flag_set!(cpu, n) == false);
		assert!(
			expected == get_r8!(cpu, a),
			"Expected {} + {} to be {} but got {}",
			l_val,
			r_val,
			expected,
			get_r8!(cpu, a)
		);

		set_r8!(cpu, a, 0x0f);
		cpu.mmu.write_mem_u8(address, 0x0f);
		reset_pc!(cpu);
		clear_flag!(cpu, h);
		cpu.run_op();
		assert!(is_flag_set!(cpu, h));

		set_r8!(cpu, a, 0xff);
		cpu.mmu.write_mem_u8(address, 0xff);
		reset_pc!(cpu);
		clear_flag!(cpu, c);
		cpu.run_op();
		assert!(is_flag_set!(cpu, c));

		set_r8!(cpu, a, 0x01);
		cpu.mmu.write_mem_u8(address, 0xff);
		reset_pc!(cpu);
		clear_flag!(cpu, z);
		cpu.run_op();
		assert!(is_flag_set!(cpu, c));

		set_r8!(cpu, a, 0x01);
		cpu.mmu.write_mem_u8(address, 0x01);
		set_flag!(cpu, c);
		reset_pc!(cpu);
		cpu.run_op();
		assert_eq!(0x03, get_r8!(cpu, a));
	}

	macro_rules! test_sub_a_r8 {
	(
		$($name:ident;$op:expr;$r8:ident;)*
	) => {
		$(
			#[test]
			fn $name() {
				let (mut cpu, mut rng) = context($op);
				let l_val: u8 = rng.gen();
				let r_val: u8 = rng.gen();

				clear_flag!(cpu, n);
				set_r8!(cpu, a, l_val);
				set_r8!(cpu, $r8, r_val);
				let cycles = cpu.run_op();
				assert_eq!(cycles, 4);
				assert_eq!(0x0101, get_r16!(cpu, pc));
				assert_eq!(l_val.wrapping_sub(r_val), get_r8!(cpu, a));
				assert!(is_flag_set!(cpu, n));

				reset_pc!(cpu);
				clear_flag!(cpu, h);
				set_r8!(cpu, a, 0x12);
				set_r8!(cpu, $r8, 0x03);
				cpu.run_op();
				assert!(is_flag_set!(cpu, h));

				reset_pc!(cpu);
				clear_flag!(cpu, c);
				set_r8!(cpu, a, 0x3);
				set_r8!(cpu, $r8, 0x13);
				cpu.run_op();
				assert!(is_flag_set!(cpu, c));

				reset_pc!(cpu);
				clear_flag!(cpu, z);
				set_r8!(cpu, a, 0x13);
				set_r8!(cpu, $r8, 0x13);
				cpu.run_op();
				assert!(is_flag_set!(cpu, z));
			}
		)*
	};
}

	test_sub_a_r8! {
		run_op_0x90; 0x90; b;
		run_op_0x91; 0x91; c;
		run_op_0x92; 0x92; d;
		run_op_0x93; 0x93; e;
		run_op_0x94; 0x94; h;
		run_op_0x95; 0x95; l;
	}

	#[test]
	fn run_op_0x96() {
		let (mut cpu, mut rng) = context(0x96);
		let l_val: u8 = rng.gen();
		let address: u16 = rng.gen_range(0x8000, 0xc000);
		let r_val: u8 = rng.gen();

		set_r16!(cpu, hl, address);

		clear_flag!(cpu, n);
		set_r8!(cpu, a, l_val);
		cpu.mmu.write_mem_u8(address, r_val);
		let cycles = cpu.run_op();
		assert_eq!(cycles, 8);
		assert_eq!(0x0101, get_r16!(cpu, pc));
		assert_eq!(l_val.wrapping_sub(r_val), get_r8!(cpu, a));
		assert!(is_flag_set!(cpu, n));

		reset_pc!(cpu);
		clear_flag!(cpu, h);
		set_r8!(cpu, a, 0x12);
		cpu.mmu.write_mem_u8(address, 0x03);
		cpu.run_op();
		assert!(is_flag_set!(cpu, h));

		reset_pc!(cpu);
		clear_flag!(cpu, c);
		set_r8!(cpu, a, 0x3);
		cpu.mmu.write_mem_u8(address, 0x13);
		cpu.run_op();
		assert!(is_flag_set!(cpu, c));

		reset_pc!(cpu);
		clear_flag!(cpu, z);
		set_r8!(cpu, a, 0x13);
		cpu.mmu.write_mem_u8(address, 0x13);
		cpu.run_op();
		assert!(is_flag_set!(cpu, z));
	}

	macro_rules! test_sbc_a_r8 {
	(
		$($name:ident;$op:expr;$r8:ident;)*
	) => {
		$(
			#[test]
			fn $name() {
				let (mut cpu, mut rng) = context($op);
				let l_val: u8 = rng.gen();
				let r_val: u8 = rng.gen();

				clear_flag!(cpu, n);
				set_r8!(cpu, a, l_val);
				set_r8!(cpu, $r8, r_val);
				let cycles = cpu.run_op();
				assert_eq!(cycles, 4);
				assert_eq!(0x0101, get_r16!(cpu, pc));
				assert_eq!(l_val.wrapping_sub(r_val), get_r8!(cpu, a));
				assert!(is_flag_set!(cpu, n));

				reset_pc!(cpu);
				clear_flag!(cpu, h);
				set_r8!(cpu, a, 0x12);
				set_r8!(cpu, $r8, 0x03);
				cpu.run_op();
				assert!(is_flag_set!(cpu, h));

				reset_pc!(cpu);
				clear_flag!(cpu, c);
				set_r8!(cpu, a, 0x3);
				set_r8!(cpu, $r8, 0x13);
				cpu.run_op();
				assert!(is_flag_set!(cpu, c));

				reset_pc!(cpu);
				clear_flag!(cpu, z);
				clear_flag!(cpu, c);
				set_r8!(cpu, a, 0x13);
				set_r8!(cpu, $r8, 0x13);
				cpu.run_op();
				assert!(is_flag_set!(cpu, z));

				reset_pc!(cpu);
				set_flag!(cpu, c);
				set_r8!(cpu, a, 0x03);
				set_r8!(cpu, $r8, 0x01);
				cpu.run_op();
				assert_eq!(0x01, get_r8!(cpu, a));
			}
		)*
	};
}

	test_sbc_a_r8! {
		run_op_0x98; 0x98; b;
		run_op_0x99; 0x99; c;
		run_op_0x9a; 0x9a; d;
		run_op_0x9b; 0x9b; e;
		run_op_0x9c; 0x9c; h;
		run_op_0x9d; 0x9d; l;
	}

	#[test]
	fn run_op_0x9e() {
		let (mut cpu, mut rng) = context(0x9e);
		let l_val: u8 = rng.gen();
		let address: u16 = rng.gen_range(0x8000, 0xc000);
		let r_val: u8 = rng.gen();

		set_r16!(cpu, hl, address);

		clear_flag!(cpu, n);
		set_r8!(cpu, a, l_val);
		cpu.mmu.write_mem_u8(address, r_val);
		let cycles = cpu.run_op();
		assert_eq!(cycles, 8);
		assert_eq!(0x0101, get_r16!(cpu, pc));
		assert_eq!(l_val.wrapping_sub(r_val), get_r8!(cpu, a));
		assert!(is_flag_set!(cpu, n));

		reset_pc!(cpu);
		clear_flag!(cpu, h);
		set_r8!(cpu, a, 0x12);
		cpu.mmu.write_mem_u8(address, 0x03);
		cpu.run_op();
		assert!(is_flag_set!(cpu, h));

		reset_pc!(cpu);
		clear_flag!(cpu, c);
		set_r8!(cpu, a, 0x3);
		cpu.mmu.write_mem_u8(address, 0x13);
		cpu.run_op();
		assert!(is_flag_set!(cpu, c));

		reset_pc!(cpu);
		clear_flag!(cpu, z);
		clear_flag!(cpu, c);
		set_r8!(cpu, a, 0x13);
		cpu.mmu.write_mem_u8(address, 0x13);
		cpu.run_op();
		assert!(is_flag_set!(cpu, z));

		reset_pc!(cpu);
		set_flag!(cpu, c);
		set_r8!(cpu, a, 0x03);
		cpu.mmu.write_mem_u8(address, 0x01);
		cpu.run_op();
		assert_eq!(0x01, get_r8!(cpu, a));
	}

	macro_rules! test_and_a_r8 {
	(
		$($name:ident;$op:expr;$r8:ident;)*
	) => {
		$(
			#[test]
			fn $name() {
				let (mut cpu, mut rng) = context($op);
				let l_val: u8 = rng.gen();
				let r_val: u8 = rng.gen();

				set_r8!(cpu, a, l_val);
				set_r8!(cpu, $r8, r_val);

				let cycles = cpu.run_op();

				assert_eq!(4, cycles);
				assert_eq!(0x0101, get_r16!(cpu, pc));
				assert_eq!(l_val & r_val, get_r8!(cpu, a));

				reset_pc!(cpu);
				set_r8!(cpu, a, 0x00);
				set_r8!(cpu, $r8, 0x00);
				clear_flag!(cpu, z);
				cpu.run_op();
				assert!(is_flag_set!(cpu, z));
			}
		)*
	};
}

	test_and_a_r8! {
		run_op_0xa0; 0xa0; b;
		run_op_0xa1; 0xa1; c;
		run_op_0xa2; 0xa2; d;
		run_op_0xa3; 0xa3; e;
		run_op_0xa4; 0xa4; h;
		run_op_0xa5; 0xa5; l;
	}

	#[test]
	fn run_op_0xa6() {
		let (mut cpu, mut rng) = context(0xa6);
		let l_val: u8 = rng.gen();
		let address: u16 = rng.gen_range(0x8000, 0xc000);
		let r_val: u8 = rng.gen();

		set_r16!(cpu, hl, address);

		set_r8!(cpu, a, l_val);
		cpu.mmu.write_mem_u8(address, r_val);

		let cycles = cpu.run_op();

		assert_eq!(8, cycles);
		assert_eq!(0x0101, get_r16!(cpu, pc));
		assert_eq!(l_val & r_val, get_r8!(cpu, a));

		reset_pc!(cpu);
		set_r8!(cpu, a, 0x00);
		cpu.mmu.write_mem_u8(address, 0x00);
		clear_flag!(cpu, z);
		cpu.run_op();
		assert!(is_flag_set!(cpu, z));
	}

	macro_rules! test_xor_a_r8 {
	(
		$($name:ident;$op:expr;$r8:ident;)*
	) => {
		$(
			#[test]
			fn $name() {
				let (mut cpu, mut rng) = context($op);
				let l_val: u8 = rng.gen();
				let r_val: u8 = rng.gen();

				set_r8!(cpu, a, l_val);
				set_r8!(cpu, $r8, r_val);

				let cycles = cpu.run_op();

				assert_eq!(4, cycles);
				assert_eq!(0x0101, get_r16!(cpu, pc));
				assert_eq!(l_val ^ r_val, get_r8!(cpu, a));

				reset_pc!(cpu);
				set_r8!(cpu, a, 0x00);
				set_r8!(cpu, $r8, 0x00);
				clear_flag!(cpu, z);
				cpu.run_op();
				assert!(is_flag_set!(cpu, z));
			}
		)*
	};
}

	test_xor_a_r8! {
		run_op_0xa8; 0xa8; b;
		run_op_0xa9; 0xa9; c;
		run_op_0xaa; 0xaa; d;
		run_op_0xab; 0xab; e;
		run_op_0xac; 0xac; h;
		run_op_0xad; 0xad; l;
	}

	#[test]
	fn run_op_0xae() {
		let (mut cpu, mut rng) = context(0xae);
		let l_val: u8 = rng.gen();
		let address: u16 = rng.gen_range(0x8000, 0xc000);
		let r_val: u8 = rng.gen();

		set_r16!(cpu, hl, address);

		set_r8!(cpu, a, l_val);
		cpu.mmu.write_mem_u8(address, r_val);

		let cycles = cpu.run_op();

		assert_eq!(8, cycles);
		assert_eq!(0x0101, get_r16!(cpu, pc));
		assert_eq!(l_val ^ r_val, get_r8!(cpu, a));

		reset_pc!(cpu);
		set_r8!(cpu, a, 0x00);
		cpu.mmu.write_mem_u8(address, 0x00);
		clear_flag!(cpu, z);
		cpu.run_op();
		assert!(is_flag_set!(cpu, z));
	}

	macro_rules! test_or_a_r8 {
	(
		$($name:ident;$op:expr;$r8:ident;)*
	) => {
		$(
			#[test]
			fn $name() {
				let (mut cpu, mut rng) = context($op);
				let l_val: u8 = rng.gen();
				let r_val: u8 = rng.gen();

				set_r8!(cpu, a, l_val);
				set_r8!(cpu, $r8, r_val);

				let cycles = cpu.run_op();

				assert_eq!(4, cycles);
				assert_eq!(0x0101, get_r16!(cpu, pc));
				assert_eq!(l_val | 	 r_val, get_r8!(cpu, a));

				reset_pc!(cpu);
				set_r8!(cpu, a, 0x00);
				set_r8!(cpu, $r8, 0x00);
				clear_flag!(cpu, z);
				cpu.run_op();
				assert!(is_flag_set!(cpu, z));
			}
		)*
	};
}

	test_or_a_r8! {
		run_op_0xb0; 0xb0; b;
		run_op_0xb1; 0xb1; c;
		run_op_0xb2; 0xb2; d;
		run_op_0xb3; 0xb3; e;
		run_op_0xb4; 0xb4; h;
		run_op_0xb5; 0xb5; l;
	}

	#[test]
	fn run_op_0xb6() {
		let (mut cpu, mut rng) = context(0xb6);
		let l_val: u8 = rng.gen();
		let address: u16 = rng.gen_range(0x8000, 0xc000);
		let r_val: u8 = rng.gen();

		set_r16!(cpu, hl, address);

		set_r8!(cpu, a, l_val);
		cpu.mmu.write_mem_u8(address, r_val);

		let cycles = cpu.run_op();

		assert_eq!(8, cycles);
		assert_eq!(0x0101, get_r16!(cpu, pc));
		assert_eq!(l_val | r_val, get_r8!(cpu, a));

		reset_pc!(cpu);
		set_r8!(cpu, a, 0x00);
		cpu.mmu.write_mem_u8(address, 0x00);
		clear_flag!(cpu, z);
		cpu.run_op();
		assert!(is_flag_set!(cpu, z));
	}

	macro_rules! test_cp_a_r8 {
	(
		$($name:ident;$op:expr;$r8:ident;)*
	) => {
		$(
			#[test]
			fn $name() {
				let (mut cpu, mut rng) = context($op);

				clear_flag!(cpu, z);
				clear_flag!(cpu, c);
				clear_flag!(cpu, h);
				clear_flag!(cpu, n);
				set_r8!(cpu, a, 0x14);
				set_r8!(cpu, $r8, 0x04);
				let cycles = cpu.run_op();
				assert_eq!(4, cycles);
				assert_eq!(0x0101, get_r16!(cpu, pc));
				assert!(false == is_flag_set!(cpu, z), "expected z to be false");
 				assert!(false == is_flag_set!(cpu, c), "expected c to be false");
 				assert!(false == is_flag_set!(cpu, h), "expected h to be false");
 				assert!(true == is_flag_set!(cpu, n), "expected n to be true");

				clear_flag!(cpu, z);
				clear_flag!(cpu, c);
				clear_flag!(cpu, h);
				clear_flag!(cpu, n);
				let val: u8 = rng.gen();
				set_r8!(cpu, a, val);
				set_r8!(cpu, $r8, val);
				reset_pc!(cpu);
				cpu.run_op();
				assert!(true == is_flag_set!(cpu, z), "expected z to be true");
				assert!(false == is_flag_set!(cpu, c), "expected c to be false");
				assert!(false == is_flag_set!(cpu, h), "expected h to be false");
				assert!(true == is_flag_set!(cpu, n), "expected n to be true");

				clear_flag!(cpu, z);
				clear_flag!(cpu, c);
				clear_flag!(cpu, h);
				clear_flag!(cpu, n);
				set_r8!(cpu, a, 0x15);
				set_r8!(cpu, $r8, 0xA0);
				reset_pc!(cpu);
				cpu.run_op();
				assert!(false == is_flag_set!(cpu, z), "expected z to be false");
				assert!(true == is_flag_set!(cpu, c), "expected c to be true");
				assert!(false == is_flag_set!(cpu, h), "expected h to be false");
				assert!(true == is_flag_set!(cpu, n), "expected n to be true");

				clear_flag!(cpu, z);
				clear_flag!(cpu, c);
				clear_flag!(cpu, h);
				clear_flag!(cpu, n);
				set_r8!(cpu, a, 0x15);
				set_r8!(cpu, $r8, 0x06);
				reset_pc!(cpu);
				cpu.run_op();
				assert!(false == is_flag_set!(cpu, z), "expected z to be false");
				assert!(false == is_flag_set!(cpu, c), "expected c to be false");
				assert!(true == is_flag_set!(cpu, h), "expected h to be true");
				assert!(true == is_flag_set!(cpu, n), "expected n to be true");
			}
		)*
	};
}

	test_cp_a_r8! {
		run_op_0xb8; 0xb8; b;
		run_op_0xb9; 0xb9; c;
		run_op_0xba; 0xba; d;
		run_op_0xbb; 0xbb; e;
		run_op_0xbc; 0xbc; h;
		run_op_0xbd; 0xbd; l;
	}

	#[test]
	fn run_op_0xbe() {
		let (mut cpu, mut rng) = context(0xbe);
		let address: u16 = rng.gen_range(0x8000, 0xC000);

		set_r16!(cpu, hl, address);

		clear_flag!(cpu, z);
		clear_flag!(cpu, c);
		clear_flag!(cpu, h);
		clear_flag!(cpu, n);
		set_r8!(cpu, a, 0x14);
		cpu.mmu.write_mem_u8(address, 0x04);
		let cycles = cpu.run_op();
		assert_eq!(4, cycles);
		assert_eq!(0x0101, get_r16!(cpu, pc));
		assert!(false == is_flag_set!(cpu, z), "expected z to be false");
		assert!(false == is_flag_set!(cpu, c), "expected c to be false");
		assert!(false == is_flag_set!(cpu, h), "expected h to be false");
		assert!(true == is_flag_set!(cpu, n), "expected n to be true");

		clear_flag!(cpu, z);
		clear_flag!(cpu, c);
		clear_flag!(cpu, h);
		clear_flag!(cpu, n);
		let val: u8 = rng.gen();
		set_r8!(cpu, a, val);
		cpu.mmu.write_mem_u8(address, val);
		reset_pc!(cpu);
		cpu.run_op();
		assert!(true == is_flag_set!(cpu, z), "expected z to be true");
		assert!(false == is_flag_set!(cpu, c), "expected c to be false");
		assert!(false == is_flag_set!(cpu, h), "expected h to be false");
		assert!(true == is_flag_set!(cpu, n), "expected n to be true");

		clear_flag!(cpu, z);
		clear_flag!(cpu, c);
		clear_flag!(cpu, h);
		clear_flag!(cpu, n);
		set_r8!(cpu, a, 0x15);
		cpu.mmu.write_mem_u8(address, 0xA0);
		reset_pc!(cpu);
		cpu.run_op();
		assert!(false == is_flag_set!(cpu, z), "expected z to be false");
		assert!(true == is_flag_set!(cpu, c), "expected c to be true");
		assert!(false == is_flag_set!(cpu, h), "expected h to be false");
		assert!(true == is_flag_set!(cpu, n), "expected n to be true");

		clear_flag!(cpu, z);
		clear_flag!(cpu, c);
		clear_flag!(cpu, h);
		clear_flag!(cpu, n);
		set_r8!(cpu, a, 0x15);
		cpu.mmu.write_mem_u8(address, 0x06);
		reset_pc!(cpu);
		cpu.run_op();
		assert!(false == is_flag_set!(cpu, z), "expected z to be false");
		assert!(false == is_flag_set!(cpu, c), "expected c to be false");
		assert!(true == is_flag_set!(cpu, h), "expected h to be true");
		assert!(true == is_flag_set!(cpu, n), "expected n to be true");
	}

	macro_rules! test_ret_f {
		(
			$($name:ident $op:expr; $flag:ident $flag_val:expr;)*
		) => {
			$(
				#[test]
				fn $name() {
					let (mut cpu, mut rng) = context($op);
					let addr: u16 = rng.gen_range(0x8000, 0xc000);

					set_r16!(cpu, sp, { 0xfffe - 2 });
					cpu.mmu.write_mem_u16(get_r16!(cpu, sp), addr);
					set_flag!(cpu, $flag, $flag_val);

					let cycles = cpu.run_op();
					assert_eq!(20, cycles);
					assert_eq!(addr, get_r16!(cpu, pc));
					assert_eq!(0xfffe, get_r16!(cpu, sp));

					let (mut cpu, mut rng) = context($op);
					let addr: u16 = rng.gen_range(0x8000, 0xc000);

					set_r16!(cpu, sp, { 0xfffe - 2 });
					cpu.mmu.write_mem_u16(get_r16!(cpu, sp), addr);
					set_flag!(cpu, $flag, {!$flag_val});

					let cycles = cpu.run_op();
					assert_eq!(8, cycles);
					assert_eq!(0x0101, get_r16!(cpu, pc));
					assert_eq!(0xfffe - 2, get_r16!(cpu, sp));

				}
			)*
		};
	}

	test_ret_f! {
		run_op_0xc0 0xc0; z false;
		run_op_0xc8 0xc8; z true;
		run_op_0xd0 0xd0; c false;
		run_op_0xd8 0xd8; c true;
	}

	macro_rules! test_pop {
		(
			$($name:ident $op:expr; $r16:ident)*
		) => {
			$(
				#[test]
				fn $name() {
					let (mut cpu, mut rng) = context($op);
					let stack_val: u16 = rng.gen();

					cpu.mmu.write_mem_u16(0xfffe - 2, stack_val);
					set_r16!(cpu, sp, {0xfffe - 2});

					let cycles = cpu.run_op();

					assert_eq!(12, cycles);
					assert_eq!(0x0101, get_r16!(cpu, pc));
					assert_eq!(0xfffe, get_r16!(cpu, sp));
					assert_eq!(stack_val, get_r16!(cpu, $r16));
				}
			)*
		};
	}

	test_pop! {
		run_op_0xc1 0xc1; bc
		run_op_0xd1 0xd1; de
		run_op_0xe1 0xe1; hl
		run_op_0xf1 0xf1; af
	}

	macro_rules! test_jp_cc_nn {
		(
			$($name:ident $op:expr; $flag:ident $flag_val:expr;)*
		) => {
			$(
				#[test]
				fn $name() {
					let (mut cpu, mut rng) = context($op);
					let next_addr: u16 = rng.gen_range(0x8000, 0xC000);

					set_flag!(cpu, $flag, $flag_val);
					cpu.mmu.write_mem_u16(0x0101, next_addr);

					let cycles = cpu.run_op();
					assert_eq!(16, cycles);
					assert_eq!(next_addr, get_r16!(cpu, pc));

					let (mut cpu, mut rng) = context($op);
					let next_addr: u16 = rng.gen_range(0x8000, 0xC000);

					set_flag!(cpu, $flag, {!$flag_val});
					cpu.mmu.write_mem_u16(0x0101, next_addr);

					let cycles = cpu.run_op();
					assert_eq!(12, cycles);
					assert_eq!(0x0103, get_r16!(cpu, pc));
				}
			)*
		};
	}

	test_jp_cc_nn! {
		run_op_0xc2 0xc2; z false;
		run_op_0xca 0xca; z true;
		run_op_0xd2 0xd2; c false;
		run_op_0xda 0xda; c true;
	}

	#[test]
	fn run_op_0xc3() {
		let (mut cpu, mut rng) = context(0xc3);
		let next_addr: u16 = rng.gen_range(0x8000, 0xC000);

		cpu.mmu.write_mem_u16(0x0101, next_addr);

		let cycles = cpu.run_op();

		assert_eq!(16, cycles);
		assert_eq!(next_addr, get_r16!(cpu, pc));
	}

	macro_rules! test_call_f_a16 {
		(
			$($name:ident $op:expr; $flag:ident $flag_val:expr;)*
		) => {
			$(
				#[test]
				fn $name() {
					let (mut cpu, mut rng) = context($op);
					let next_addr: u16 = rng.gen_range(0x8000, 0xC000);

					cpu.mmu.write_mem_u16(0x0101, next_addr);
					set_flag!(cpu, $flag, $flag_val);

					let cycles = cpu.run_op();

					assert_eq!(24, cycles);
					assert_eq!(next_addr, get_r16!(cpu, pc));
					assert_eq!(0xfffe - 2, get_r16!(cpu, sp));
					assert_eq!(0x0103, cpu.mmu.read_mem_u16(get_r16!(cpu, sp)));

					let (mut cpu, mut rng) = context($op);
					let next_addr: u16 = rng.gen_range(0x8000, 0xC000);

					cpu.mmu.write_mem_u16(0x0101, next_addr);
					set_flag!(cpu, $flag, {!$flag_val});

					let cycles = cpu.run_op();

					assert_eq!(12, cycles);
					assert_eq!(0xfffe, get_r16!(cpu, sp));
					assert_eq!(0x0103, get_r16!(cpu, pc));
				}
			)*
		};
	}

	test_call_f_a16! {
		run_op_0xc4 0xc4; z false;
		run_op_0xcc 0xcc; z true;
		run_op_0xd4 0xd4; c false;
		run_op_0xdc 0xdc; c true;
	}

	macro_rules! test_push {
		(
			$($name:ident $op:expr; $r16:ident)*
		) => {
			$(
				#[test]
				fn $name() {
					let (mut cpu, mut rng) = context($op);
					let val: u16 = rng.gen();

					set_r16!(cpu, $r16, val);

					let cycles = cpu.run_op();

					assert_eq!(16, cycles);
					assert_eq!(0x0101, get_r16!(cpu, pc));
					assert_eq!(0xfffe - 2, get_r16!(cpu, sp));
					assert_eq!(val, cpu.mmu.read_mem_u16(0xfffe - 2));
				}
			)*
		};
	}

	test_push! {
		run_op_0xc5 0xc5; bc
		run_op_0xd5 0xd5; de
		run_op_0xe5 0xe5; hl
		run_op_0xf5 0xf5; af
	}

	#[test]
	fn run_op_0xc9() {
		let (mut cpu, mut rng) = context(0xc9);
		let addr: u16 = rng.gen_range(0x8000, 0xc000);

		set_r16!(cpu, sp, { 0xfffe - 2 });
		cpu.mmu.write_mem_u16(get_r16!(cpu, sp), addr);

		let cycles = cpu.run_op();
		assert_eq!(16, cycles);
		assert_eq!(addr, get_r16!(cpu, pc));
		assert_eq!(0xfffe, get_r16!(cpu, sp));
	}

	#[test]
	fn run_op_0xcd() {
		let (mut cpu, mut rng) = context(0xcd);
		let next_addr: u16 = rng.gen_range(0x8000, 0xC000);

		cpu.mmu.write_mem_u16(0x0101, next_addr);

		let cycles = cpu.run_op();

		assert_eq!(24, cycles);
		assert_eq!(next_addr, get_r16!(cpu, pc));
		assert_eq!(0xfffe - 2, get_r16!(cpu, sp));
		assert_eq!(0x0103, cpu.mmu.read_mem_u16(get_r16!(cpu, sp)));
	}

	// #[test]
	#[allow(dead_code)]
	fn all_ops_implemented() {
		let mut skip = HashSet::new();

		skip.insert(0xcb);
		skip.insert(0xd3);
		skip.insert(0xd9); // interrupts...
		skip.insert(0xdb);
		skip.insert(0xdd);
		skip.insert(0xe3);
		skip.insert(0xe4);
		skip.insert(0xeb);
		skip.insert(0xec);
		skip.insert(0xed);
		skip.insert(0xf4);
		skip.insert(0xfc);
		skip.insert(0xfd);

		for opcode in 0x00..=0xff {
			if !skip.contains(&opcode) {
				let mut cpu = Cpu::new();
				cpu.mmu.write_mem_u8(0x0100, opcode);
				cpu.run_op();
			}
		}

		for rotate in 0x00..=0xff {
			let mut cpu = Cpu::new();
			cpu.mmu.write_mem_u8(0x0100, 0xcb);
			cpu.mmu.write_mem_u8(0x0101, rotate);
			cpu.run_op();
		}
	}

}
