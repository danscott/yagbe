macro_rules! inc_pc {
	($cpu:ident) => {
		$cpu.registers.pc += 1;
	};
	($cpu:ident by $amount:expr) => {
		$cpu.registers.pc += $amount;
	};
}

macro_rules! set_r8 {
	($cpu:ident,a,$val:tt) => {
		set_top!($cpu, af, $val);
	};
	($cpu:ident,f,$val:tt) => {
		set_bottom!($cpu, af, $val);
	};
	($cpu:ident,b,$val:tt) => {
		set_top!($cpu, bc, $val);
	};
	($cpu:ident,c,$val:tt) => {
		set_bottom!($cpu, bc, $val);
	};
	($cpu:ident,d,$val:tt) => {
		set_top!($cpu, de, $val);
	};
	($cpu:ident,e,$val:tt) => {
		set_bottom!($cpu, de, $val);
	};
	($cpu:ident,h,$val:tt) => {
		set_top!($cpu, hl, $val);
	};
	($cpu:ident,l,$val:tt) => {
		set_bottom!($cpu, hl, $val);
	};
}

macro_rules! get_r8 {
	($cpu:ident,a) => {
		get_top!($cpu, af);
	};
	($cpu:ident,f) => {
		get_bottom!($cpu, af);
	};
	($cpu:ident,b) => {
		get_top!($cpu, bc);
	};
	($cpu:ident,c) => {
		get_bottom!($cpu, bc);
	};
	($cpu:ident,d) => {
		get_top!($cpu, de);
	};
	($cpu:ident,e) => {
		get_bottom!($cpu, de);
	};
	($cpu:ident,h) => {
		get_top!($cpu, hl);
	};
	($cpu:ident,l) => {
		get_bottom!($cpu, hl);
	};
}

macro_rules! set_r16 {
	($cpu:ident,sp,$val:tt) => {
		$cpu.registers.sp = $val;
	};
	($cpu:ident,pc,$val:tt) => {
		$cpu.registers.pc = $val;
	};
	($cpu:ident,$reg:ident,$val:tt) => {
		$cpu.registers.$reg.value = $val;
	};
}

macro_rules! get_r16 {
	($cpu:ident,sp) => {
		$cpu.registers.sp
	};
	($cpu:ident,pc) => {
		$cpu.registers.pc
	};
	($cpu:ident,$r16:ident) => {
		unsafe { $cpu.registers.$r16.value }
	};
}

macro_rules! set_top {
	($cpu:ident,$reg:ident,$val:tt) => {
		unsafe {
			$cpu.registers.$reg.parts.top = $val;
			}
	};
}

macro_rules! set_bottom {
	($cpu:ident,$reg:ident,$val:tt) => {
		unsafe {
			$cpu.registers.$reg.parts.bottom = $val;
			}
	};
}

macro_rules! get_top {
	($cpu:ident,$reg:ident) => {
		unsafe { $cpu.registers.$reg.parts.top }
	};
}

macro_rules! get_bottom {
	($cpu:ident,$reg:ident) => {
		unsafe { $cpu.registers.$reg.parts.bottom }
	};
}

#[derive(Clone, Copy)]
#[repr(C)]
pub struct U8Pair {
	pub bottom: u8,
	pub top: u8,
}

pub union RegU16 {
	pub parts: U8Pair,
	pub value: u16,
}

pub struct Registers {
	pub af: RegU16,
	pub bc: RegU16,
	pub de: RegU16,
	pub hl: RegU16,
	pub sp: u16,
	pub pc: u16,
}

impl Registers {
	pub fn new() -> Registers {
		Registers {
			af: RegU16 { value: 0x00 },
			bc: RegU16 { value: 0x00 },
			de: RegU16 { value: 0x00 },
			hl: RegU16 { value: 0x00 },
			sp: 0xfffe,
			pc: 0x0100,
		}
	}
}
