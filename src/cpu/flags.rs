macro_rules! set_flag {
  ($cpu:ident,$flag:ident) => {
    $cpu.flags.$flag = true;
  };
  ($cpu:ident,$flag:ident,$val:tt) => {
    $cpu.flags.$flag = $val;
  };
}

macro_rules! clear_flag {
  ($cpu:ident,$flag:ident) => {
    $cpu.flags.$flag = false;
  };
}

macro_rules! is_flag_set {
  ($cpu:ident,$flag:ident) => {
    $cpu.flags.$flag
  };
}

pub struct Flags {
  pub z: bool,
  pub n: bool,
  pub h: bool,
  pub c: bool,
}

impl Flags {
  pub fn new() -> Flags {
    Flags {
      z: false,
      n: false,
      h: false,
      c: false,
    }
  }
}
