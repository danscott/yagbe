use cpu::flags::Flags;
use cpu::memory::Mmu;
use cpu::registers::Registers;

macro_rules! read_code {
	(u8 into $out:ident from $cpu:ident) => {
		let $out = $cpu.mmu.read_mem_u8(get_r16!($cpu, pc));
		inc_pc!($cpu);
	};

	(u16 into $out:ident from $cpu:ident) => {
		let $out = $cpu.mmu.read_mem_u16(get_r16!($cpu, pc));
		inc_pc!($cpu by 2);
	};
}

macro_rules! push {
  ($cpu:ident from $r16:ident) => {{
    let val = get_r16!($cpu, $r16);
    push!($cpu u16 val);
    return 16;
  }};
  ($cpu:ident u16 $val:expr) => {
    let next_sp = get_r16!($cpu, sp) - 2;
    set_r16!($cpu, sp, next_sp);
    $cpu.mmu.write_mem_u16(next_sp, $val);
  };
}

macro_rules! pop {
  ($cpu:ident into $r16:ident) => {{
    pop!($cpu into u16 stack_val);
    set_r16!($cpu, $r16, stack_val);
    return 12;
  }};
  ($cpu:ident into u16 $target:ident) => {
    let sp_val = get_r16!($cpu, sp);
    let $target = $cpu.mmu.read_mem_u16(sp_val);
    set_r16!($cpu, sp, { sp_val + 2 });
  };
}

macro_rules! ld_r16_nn {
  ($cpu:ident,$r16:ident) => {{
    read_code!(u16 into mem_val from $cpu);
    set_r16!($cpu, $r16, mem_val);
    return 12;
  }};
}

macro_rules! ld_r8_d8 {
  ($cpu:ident,$r8:ident) => {{
    read_code!(u8 into val from $cpu);
    set_r8!($cpu, $r8, val);
    return 8;
  }};
}

macro_rules! ld_r16p_r8 {
  ($cpu:ident,$r16:ident,$r8:ident) => {{
    let address = get_r16!($cpu, $r16);
    let val = get_r8!($cpu, $r8);
    $cpu.mmu.write_mem_u8(address, val);
    return 8;
  }};
}

macro_rules! ld_hlop_a {
  ($cpu:ident inc) => {{
    let address = get_r16!($cpu, hl);
    let val = get_r8!($cpu, a);
    $cpu.mmu.write_mem_u8(address, val);
    set_r16!($cpu, hl, { address.wrapping_add(1) });
    return 8;
  }};
  ($cpu:ident dec) => {{
    let address = get_r16!($cpu, hl);
    let val = get_r8!($cpu, a);
    $cpu.mmu.write_mem_u8(address, val);
    set_r16!($cpu, hl, { address.wrapping_sub(1) });
    return 8;
  }};
}

macro_rules! ld_a_hlop {
  ($cpu:ident inc) => {{
    let address = get_r16!($cpu, hl);
    let val = $cpu.mmu.read_mem_u8(address);
    set_r8!($cpu, a, val);
    set_r16!($cpu, hl, { address.wrapping_add(1) });
    return 8;
  }};
  ($cpu:ident dec) => {{
    let address = get_r16!($cpu, hl);
    let val = $cpu.mmu.read_mem_u8(address);
    set_r8!($cpu, a, val);
    set_r16!($cpu, hl, { address.wrapping_sub(1) });
    return 8;
  }};
}

macro_rules! ld_a16p_sp {
  ($cpu:ident) => {{
    read_code!(u16 into address from $cpu);
    $cpu.mmu.write_mem_u16(address, get_r16!($cpu, sp));
    return 20;
  }};
}

macro_rules! inc_r16 {
  ($cpu:ident,$r16:ident) => {{
    let val = get_r16!($cpu, $r16);
    let (next, _) = val.overflowing_add(1);
    set_r16!($cpu, $r16, next);
    return 8;
  }};
}

macro_rules! dec_r16 {
  ($cpu:ident,$r16:ident) => {{
    let val = get_r16!($cpu, $r16);
    let (next, _) = val.overflowing_sub(1);
    set_r16!($cpu, $r16, next);
    return 8;
  }};
}

macro_rules! inc {
  ($cpu:ident,$val:expr, into $next:ident) => {
    set_flag!($cpu, h, { ($val & 0x0f) == 0x0f });
    let ($next, overflow) = $val.overflowing_add(1);
    set_flag!($cpu, z, { overflow });
    clear_flag!($cpu, n);
  };
}

macro_rules! dec {
  ($cpu:ident,$val:expr,into $next:ident) => {
    set_flag!($cpu, h, { ($val & 0x0f) == 0x00 });
    let ($next, _) = $val.overflowing_sub(1);
    set_flag!($cpu, z, { $next == 0 });
    set_flag!($cpu, n);
  };
}

macro_rules! inc_r8 {
  ($cpu:ident,$r8:ident) => {{
    let val = get_r8!($cpu, $r8);
    inc!($cpu, val, into next);
    set_r8!($cpu, $r8, next);
    return 4;
  }};
}

macro_rules! dec_r8 {
  ($cpu:ident,$r8:ident) => {{
    let val = get_r8!($cpu, $r8);
    dec!($cpu, val, into next);
    set_r8!($cpu, $r8, next);
    return 4;
  }};
}

macro_rules! inc_hlp {
  ($cpu:ident) => {{
    let address = get_r16!($cpu, hl);
    let val = $cpu.mmu.read_mem_u8(address);
    inc!($cpu, val, into next);
    $cpu.mmu.write_mem_u8(address, next);
    return 12;
  }};
}

macro_rules! dec_hlp {
  ($cpu:ident) => {{
    let address = get_r16!($cpu, hl);
    let val = $cpu.mmu.read_mem_u8(address);
    dec!($cpu, val, into next);
    $cpu.mmu.write_mem_u8(address, next);
    return 12;
  }};
}

macro_rules! rlca {
  ($cpu:ident) => {{
    let val = get_r8!($cpu, a);
    set_flag!($cpu, c, { (val & 0x80) == 0x80 });
    set_r8!($cpu, a, { val.rotate_left(1) });
    clear_flag!($cpu, h);
    clear_flag!($cpu, n);
    clear_flag!($cpu, z);
    return 4;
  }};
}

macro_rules! rla {
  ($cpu:ident) => {{
    let val = get_r8!($cpu, a);
    let carry = if is_flag_set!($cpu, c) { 1 } else { 0 };
    set_flag!($cpu, c, { (val & 0x80) == 0x80 });
    set_r8!($cpu, a, { val.wrapping_shl(1) | carry });
    clear_flag!($cpu, h);
    clear_flag!($cpu, n);
    clear_flag!($cpu, z);
    return 4;
  }};
}

macro_rules! rrca {
  ($cpu:ident) => {{
    let val = get_r8!($cpu, a);
    set_flag!($cpu, c, { (val & 0x01) == 0x01 });
    set_r8!($cpu, a, { val.rotate_right(1) });
    clear_flag!($cpu, h);
    clear_flag!($cpu, n);
    clear_flag!($cpu, z);
    return 4;
  }};
}

macro_rules! rra {
  ($cpu:ident) => {{
    let val = get_r8!($cpu, a);
    let next = if is_flag_set!($cpu, c) {
      (val >> 1) & 0x80
    } else {
      (val >> 1)
    };
    set_flag!($cpu, c, { (val & 0x01) == 0x01 });
    set_r8!($cpu, a, next);
    clear_flag!($cpu, h);
    clear_flag!($cpu, n);
    clear_flag!($cpu, z);
    return 4;
  }};
}

macro_rules! add_hl_r16 {
  ($cpu:ident) => {{
    let lhs: u32 = get_r16!($cpu, hl).into();
    let result = lhs + lhs;
    set_flag!($cpu, c, { (0xffff0000 & result) > 0 });
    set_flag!($cpu, h, { ((0x0f & lhs) == 0x0f) && lhs > 0 });
    clear_flag!($cpu, n);
    set_r16!($cpu, hl, { result as u16 });
    return 8;
  }};
  ($cpu:ident,$r16:ident) => {{
    let lhs: u32 = get_r16!($cpu, hl).into();
    let rhs: u32 = get_r16!($cpu, $r16).into();
    let result = lhs + rhs;
    set_flag!($cpu, c, { (0xffff0000 & result) > 0 });
    set_flag!($cpu, h, { ((0x0f & lhs) == 0x0f) && rhs > 0 });
    clear_flag!($cpu, n);
    set_r16!($cpu, hl, { result as u16 });
    return 8;
  }};
}

macro_rules! add_u8_u8_to_a {
  ($cpu:ident,$l_val:ident,$r_val:ident with carry) => {
    let carry = if is_flag_set!($cpu, c) { 1 } else { 0 };
    let next = $l_val + $r_val + carry;
    set_flag!($cpu, h, { ($l_val & 0x0f) + ($r_val & 0x0f) + 1 > 0x0f });
    add_u8_u8_to_a!($cpu, next)
  };
  ($cpu:ident,$l_val:ident,$r_val:ident) => {
    let next = $l_val + $r_val;
    set_flag!($cpu, h, { ($l_val & 0x0f) + ($r_val & 0x0f) > 0x0f });
    add_u8_u8_to_a!($cpu, next)
  };
  ($cpu:ident,$next:ident) => {
    set_r8!($cpu, a, { $next as u8 });
    set_flag!($cpu, c, { ($next & 0xff00) > 0 });
    clear_flag!($cpu, n);
    set_flag!($cpu, z, { $next as u8 == 0 });
  };
}

macro_rules! add_a_r8 {
  ($cpu:ident,$r8:ident) => {{
    let l_val = get_r8!($cpu, a) as u16;
    let r_val = get_r8!($cpu, $r8) as u16;
    add_u8_u8_to_a!($cpu, l_val, r_val);
    return 4;
  }};
}

macro_rules! add_a_hlp {
  ($cpu:ident) => {{
    let address = get_r16!($cpu, hl);
    let r_val = $cpu.mmu.read_mem_u8(address) as u16;
    let l_val = get_r8!($cpu, a) as u16;
    add_u8_u8_to_a!($cpu, l_val, r_val);
    return 8;
  }};
}

macro_rules! adc_a_r8 {
  ($cpu:ident,$r8:ident) => {{
    let l_val = get_r8!($cpu, a) as u16;
    let r_val = get_r8!($cpu, $r8) as u16;
    add_u8_u8_to_a!($cpu, l_val, r_val with carry);
    return 4;
  }};
}

macro_rules! adc_a_hlp {
  ($cpu:ident) => {{
    let l_val = get_r8!($cpu, a) as u16;
    let address = get_r16!($cpu, hl);
    let r_val = $cpu.mmu.read_mem_u8(address) as u16;
    add_u8_u8_to_a!($cpu, l_val, r_val with carry);
    return 4;
  }};
}

macro_rules! sub_u8_u8_to_a {
	($cpu:ident,$l_val:ident,$r_val:ident with carry) => {
		let carry = if is_flag_set!($cpu, c) { 1 } else { 0 };
		let (next, overflow) = $l_val.overflowing_sub($r_val + carry);
		set_flag!($cpu, h, { ($l_val & 0x0f) < (($r_val + carry) & 0x0f) });
		sub_u8_u8_to_a!($cpu, next with overflow);
	};
	($cpu:ident,$l_val:ident,$r_val:ident) => {
		let (next, overflow) = $l_val.overflowing_sub($r_val);
		set_flag!($cpu, h, { ($l_val & 0x0f) < ($r_val & 0x0f) });
		sub_u8_u8_to_a!($cpu, next with overflow);
	};
	($cpu:ident,$next:ident with $overflow:ident ) => {
		set_r8!($cpu, a, $next);
		set_flag!($cpu, n);
		set_flag!($cpu, c, { $overflow });
		set_flag!($cpu, z, { $next == 0 });
	};
}

macro_rules! sub_r8 {
  ($cpu:ident,$r8:ident) => {{
    let l_val = get_r8!($cpu, a);
    let r_val = get_r8!($cpu, $r8);
    sub_u8_u8_to_a!($cpu, l_val, r_val);
    return 4;
  }};
}

macro_rules! sub_hlp {
  ($cpu:ident) => {{
    let address = get_r16!($cpu, hl);
    let r_val = $cpu.mmu.read_mem_u8(address);
    let l_val = get_r8!($cpu, a);
    sub_u8_u8_to_a!($cpu, l_val, r_val);
    return 8;
  }};
}

macro_rules! sbc_r8 {
  ($cpu:ident,$r8:ident) => {{
    let l_val = get_r8!($cpu, a);
    let r_val = get_r8!($cpu, $r8);
    sub_u8_u8_to_a!($cpu, l_val, r_val with carry);
    return 4;
  }};
}

macro_rules! sbc_hlp {
  ($cpu:ident) => {{
    let address = get_r16!($cpu, hl);
    let r_val = $cpu.mmu.read_mem_u8(address);
    let l_val = get_r8!($cpu, a);
    sub_u8_u8_to_a!($cpu, l_val, r_val with carry);
    return 8;
  }};
}

macro_rules! ld_r8_r16p {
  ($cpu:ident,$r8:ident,$r16:ident) => {{
    let address = get_r16!($cpu, $r16);
    let val = $cpu.mmu.read_mem_u8(address);
    set_r8!($cpu, $r8, val);
    return 8;
  }};
}

macro_rules! ld_hlp_d8 {
  ($cpu:ident) => {{
    read_code!(u8 into val from $cpu);
    let address = get_r16!($cpu, hl);
    $cpu.mmu.write_mem_u8(address, val);
    return 12;
  }};
}

macro_rules! ld_r8_r8 {
  ($cpu:ident,$r8_to:ident,$r8_from:ident) => {{
    let val = get_r8!($cpu, $r8_from);
    set_r8!($cpu, $r8_to, val);
    return 4;
  }};
}

macro_rules! add_jp_val {
  ($cpu:ident,$jp_len:ident) => {
    $cpu.registers.pc = (($cpu.registers.pc as i32) + (($jp_len as i8) as i32)) as u16;
  };
}

macro_rules! jr {
  ($cpu:ident) => {{
    read_code!(u8 into jp_len from $cpu);
    add_jp_val!($cpu, jp_len);
    return 12;
  }};
  ($cpu:ident when $flag:ident is $flag_val:expr) => {{
    read_code!(u8 into jp_len from $cpu);
    if is_flag_set!($cpu, $flag) == $flag_val {
      add_jp_val!($cpu, jp_len);
      return 12;
    }
    return 8;
  }};
}

macro_rules! jp {
  ($cpu:ident) => {{
    read_code!(u16 into jp_to from $cpu);
    set_r16!($cpu, pc, jp_to);
    return 16;
  }};
  ($cpu:ident when $flag:ident is $flag_val:expr) => {{
    read_code!(u16 into jp_to from $cpu);
    if is_flag_set!($cpu, $flag) == $flag_val {
      set_r16!($cpu, pc, jp_to);
      return 16;
    }
    return 12;
  }};
}

macro_rules! and_r8 {
  ($cpu:ident,$r8:ident) => {{
    let next = get_r8!($cpu, a) & get_r8!($cpu, $r8);
    set_r8!($cpu, a, next);
    set_flag!($cpu, h);
    clear_flag!($cpu, n);
    clear_flag!($cpu, c);
    set_flag!($cpu, z, { next == 0 });
    return 4;
  }};
}

macro_rules! and_hlp {
  ($cpu:ident) => {{
    let address = get_r16!($cpu, hl);
    let next = get_r8!($cpu, a) & $cpu.mmu.read_mem_u8(address);
    set_r8!($cpu, a, next);
    set_flag!($cpu, h);
    clear_flag!($cpu, n);
    clear_flag!($cpu, c);
    set_flag!($cpu, z, { next == 0 });
    return 8;
  }};
}

macro_rules! xor_r8 {
  ($cpu:ident,$r8:ident) => {{
    let next = get_r8!($cpu, a) ^ get_r8!($cpu, $r8);
    set_r8!($cpu, a, next);
    clear_flag!($cpu, h);
    clear_flag!($cpu, n);
    clear_flag!($cpu, c);
    set_flag!($cpu, z, { next == 0 });
    return 4;
  }};
}

macro_rules! xor_hlp {
  ($cpu:ident) => {{
    let address = get_r16!($cpu, hl);
    let next = get_r8!($cpu, a) ^ $cpu.mmu.read_mem_u8(address);
    set_r8!($cpu, a, next);
    set_flag!($cpu, h);
    clear_flag!($cpu, n);
    clear_flag!($cpu, c);
    set_flag!($cpu, z, { next == 0 });
    return 8;
  }};
}

macro_rules! or_r8 {
  ($cpu:ident,$r8:ident) => {{
    let next = get_r8!($cpu, a) | get_r8!($cpu, $r8);
    set_r8!($cpu, a, next);
    clear_flag!($cpu, h);
    clear_flag!($cpu, n);
    clear_flag!($cpu, c);
    set_flag!($cpu, z, { next == 0 });
    return 4;
  }};
}

macro_rules! or_hlp {
  ($cpu:ident) => {{
    let address = get_r16!($cpu, hl);
    let next = get_r8!($cpu, a) | $cpu.mmu.read_mem_u8(address);
    set_r8!($cpu, a, next);
    set_flag!($cpu, h);
    clear_flag!($cpu, n);
    clear_flag!($cpu, c);
    set_flag!($cpu, z, { next == 0 });
    return 8;
  }};
}

macro_rules! cp_r8 {
  ($cpu:ident,$r8:ident) => {{
    let l_val = get_r8!($cpu, a);
    let r_val = get_r8!($cpu, $r8);
    set_flag!($cpu, n);
    set_flag!($cpu, z, { l_val == r_val });
    set_flag!($cpu, c, { l_val < r_val });
    set_flag!($cpu, h, { (l_val & 0x0f) < (r_val & 0x0f) });
    return 4;
  }};
}

macro_rules! cp_hlp {
  ($cpu:ident) => {{
    let l_val = get_r8!($cpu, a);
    let address = get_r16!($cpu, hl);
    let r_val = $cpu.mmu.read_mem_u8(address);
    set_flag!($cpu, n);
    set_flag!($cpu, z, { l_val == r_val });
    set_flag!($cpu, c, { l_val < r_val });
    set_flag!($cpu, h, { (l_val & 0x0f) < (r_val & 0x0f) });
    return 4;
  }};
}

macro_rules! daa {
  ($cpu:ident) => {{
    let mut value = get_r8!($cpu, a);
    let n = is_flag_set!($cpu, n);
    let mut set_c = false;
    let mut correction: u8 = 0x00;
    if is_flag_set!($cpu, h) || (!n && (value & 0x0f) > 9) {
      correction |= 0x06;
    }

    if is_flag_set!($cpu, c) || (!n && value > 0x99) {
      correction |= 0x60;
      set_c = true;
    }

    value = if n {
      value - correction
    } else {
      (value as u16 + correction as u16) as u8
    };

    set_r8!($cpu, a, value);
    set_flag!($cpu, z, { value == 0 });
    set_flag!($cpu, c, { set_c });
    clear_flag!($cpu, h);

    return 4;
  }};
}

macro_rules! stop {
  ($cpu:ident) => {{
    $cpu.registers.pc += 1;
    $cpu.stopped = true;
    return 4;
  }};
}

macro_rules! halt {
  ($cpu:ident) => {{
    $cpu.halted = true;
    return 1;
  }};
}

macro_rules! cpl {
  ($cpu:ident) => {{
    let val = get_r8!($cpu, a);
    set_r8!($cpu, a, { !val });
    return 4;
  }};
}

macro_rules! scf {
  ($cpu:ident) => {{
    set_flag!($cpu, c);
    clear_flag!($cpu, h);
    clear_flag!($cpu, n);
    return 4;
  }};
}

macro_rules! ccf {
  ($cpu:ident) => {{
    set_flag!($cpu, c, { !is_flag_set!($cpu, c) });
    clear_flag!($cpu, h);
    clear_flag!($cpu, n);
    return 4;
  }};
}

macro_rules! call {
  ($cpu:ident a16) => {{
    read_code!(u16 into next_addr from $cpu);
    call!($cpu, next_addr);
  }};
  ($cpu:ident when $flag:ident is $flag_val:expr) => {{
    read_code!(u16 into next_addr from $cpu);
    if is_flag_set!($cpu, $flag) == $flag_val {
      call!($cpu, next_addr);
    }
    return 12;
  }};
  ($cpu:ident, $operand:expr) => {
    push!($cpu u16 get_r16!($cpu, pc));
    set_r16!($cpu, pc, $operand);
    return 24;
  };
}

macro_rules! ret {
  ($cpu:ident) => {{
    pop!($cpu into u16 new_pc);
    set_r16!($cpu, pc, new_pc);
    return 16;
  }};
  ($cpu:ident when $flag:ident is $flag_val:expr) => {{
    if is_flag_set!($cpu, $flag) == $flag_val {
      pop!($cpu into u16 new_pc);
      set_r16!($cpu, pc, new_pc);
      return 20;
    }
    return 8;
  }};
}

pub struct Cpu {
  pub mmu: Mmu,
  pub registers: Registers,
  pub flags: Flags,
  pub stopped: bool,
  pub halted: bool,
}

impl Cpu {
  pub fn new() -> Cpu {
    Cpu {
      mmu: Mmu::new(),
      registers: Registers::new(),
      flags: Flags::new(),
      stopped: false,
      halted: false,
    }
  }

  pub fn run_op(&mut self) -> usize {
    read_code!(u8 into opcode from self);

    match opcode {
      0x00 => 4,
      0x01 => ld_r16_nn!(self, bc),
      0x02 => ld_r16p_r8!(self, bc, a),
      0x03 => inc_r16!(self, bc),
      0x04 => inc_r8!(self, b),
      0x05 => dec_r8!(self, b),
      0x06 => ld_r8_d8!(self, b),
      0x07 => rlca!(self),

      0x08 => ld_a16p_sp!(self),
      0x09 => add_hl_r16!(self, bc),
      0x0a => ld_r8_r16p!(self, a, bc),
      0x0b => dec_r16!(self, bc),
      0x0c => inc_r8!(self, c),
      0x0d => dec_r8!(self, c),
      0x0e => ld_r8_d8!(self, c),
      0x0f => rrca!(self),

      0x10 => stop!(self),
      0x11 => ld_r16_nn!(self, de),
      0x12 => ld_r16p_r8!(self, de, a),
      0x13 => inc_r16!(self, de),
      0x14 => inc_r8!(self, d),
      0x15 => dec_r8!(self, d),
      0x16 => ld_r8_d8!(self, d),
      0x17 => rla!(self),

      0x18 => jr!(self),
      0x19 => add_hl_r16!(self, de),
      0x1a => ld_r8_r16p!(self, a, de),
      0x1b => dec_r16!(self, de),
      0x1c => inc_r8!(self, e),
      0x1d => dec_r8!(self, e),
      0x1e => ld_r8_d8!(self, e),
      0x1f => rra!(self),

      0x20 => jr!(self when z is false),
      0x21 => ld_r16_nn!(self, hl),
      0x22 => ld_hlop_a!(self inc),
      0x23 => inc_r16!(self, hl),
      0x24 => inc_r8!(self, h),
      0x25 => dec_r8!(self, h),
      0x26 => ld_r8_d8!(self, h),
      0x27 => daa!(self),

      0x28 => jr!(self when z is true),
      0x29 => add_hl_r16!(self),
      0x2a => ld_a_hlop!(self inc),
      0x2b => dec_r16!(self, hl),
      0x2c => inc_r8!(self, l),
      0x2d => dec_r8!(self, l),
      0x2e => ld_r8_d8!(self, l),
      0x2f => cpl!(self),

      0x30 => jr!(self when c is false),
      0x31 => ld_r16_nn!(self, sp),
      0x32 => ld_hlop_a!(self dec),
      0x33 => inc_r16!(self, sp),
      0x34 => inc_hlp!(self),
      0x35 => dec_hlp!(self),
      0x36 => ld_hlp_d8!(self),
      0x37 => scf!(self),

      0x38 => jr!(self when c is true),
      0x39 => add_hl_r16!(self, sp),
      0x3a => ld_a_hlop!(self dec),
      0x3b => dec_r16!(self, sp),
      0x3c => inc_r8!(self, a),
      0x3d => dec_r8!(self, a),
      0x3e => ld_r8_d8!(self, a),
      0x3f => ccf!(self),

      0x40 => ld_r8_r8!(self, b, b),
      0x41 => ld_r8_r8!(self, b, c),
      0x42 => ld_r8_r8!(self, b, d),
      0x43 => ld_r8_r8!(self, b, e),
      0x44 => ld_r8_r8!(self, b, h),
      0x45 => ld_r8_r8!(self, b, l),
      0x46 => ld_r8_r16p!(self, b, hl),
      0x47 => ld_r8_r8!(self, b, a),

      0x48 => ld_r8_r8!(self, c, b),
      0x49 => ld_r8_r8!(self, c, c),
      0x4a => ld_r8_r8!(self, c, d),
      0x4b => ld_r8_r8!(self, c, e),
      0x4c => ld_r8_r8!(self, c, h),
      0x4d => ld_r8_r8!(self, c, l),
      0x4e => ld_r8_r16p!(self, c, hl),
      0x4f => ld_r8_r8!(self, c, a),

      0x50 => ld_r8_r8!(self, d, b),
      0x51 => ld_r8_r8!(self, d, c),
      0x52 => ld_r8_r8!(self, d, d),
      0x53 => ld_r8_r8!(self, d, e),
      0x54 => ld_r8_r8!(self, d, h),
      0x55 => ld_r8_r8!(self, d, l),
      0x56 => ld_r8_r16p!(self, d, hl),
      0x57 => ld_r8_r8!(self, d, a),

      0x58 => ld_r8_r8!(self, e, b),
      0x59 => ld_r8_r8!(self, e, c),
      0x5a => ld_r8_r8!(self, e, d),
      0x5b => ld_r8_r8!(self, e, e),
      0x5c => ld_r8_r8!(self, e, h),
      0x5d => ld_r8_r8!(self, e, l),
      0x5e => ld_r8_r16p!(self, e, hl),
      0x5f => ld_r8_r8!(self, e, a),

      0x60 => ld_r8_r8!(self, h, b),
      0x61 => ld_r8_r8!(self, h, c),
      0x62 => ld_r8_r8!(self, h, d),
      0x63 => ld_r8_r8!(self, h, e),
      0x64 => ld_r8_r8!(self, h, h),
      0x65 => ld_r8_r8!(self, h, l),
      0x66 => ld_r8_r16p!(self, h, hl),
      0x67 => ld_r8_r8!(self, h, a),

      0x68 => ld_r8_r8!(self, l, b),
      0x69 => ld_r8_r8!(self, l, c),
      0x6a => ld_r8_r8!(self, l, d),
      0x6b => ld_r8_r8!(self, l, e),
      0x6c => ld_r8_r8!(self, l, h),
      0x6d => ld_r8_r8!(self, l, l),
      0x6e => ld_r8_r16p!(self, l, hl),
      0x6f => ld_r8_r8!(self, l, a),

      0x70 => ld_r16p_r8!(self, hl, b),
      0x71 => ld_r16p_r8!(self, hl, c),
      0x72 => ld_r16p_r8!(self, hl, d),
      0x73 => ld_r16p_r8!(self, hl, e),
      0x74 => ld_r16p_r8!(self, hl, h),
      0x75 => ld_r16p_r8!(self, hl, l),
      0x76 => halt!(self),
      0x77 => ld_r16p_r8!(self, hl, a),

      0x78 => ld_r8_r8!(self, a, b),
      0x79 => ld_r8_r8!(self, a, c),
      0x7a => ld_r8_r8!(self, a, d),
      0x7b => ld_r8_r8!(self, a, e),
      0x7c => ld_r8_r8!(self, a, h),
      0x7d => ld_r8_r8!(self, a, l),
      0x7e => ld_r8_r16p!(self, a, hl),
      0x7f => ld_r8_r8!(self, a, a),

      0x80 => add_a_r8!(self, b),
      0x81 => add_a_r8!(self, c),
      0x82 => add_a_r8!(self, d),
      0x83 => add_a_r8!(self, e),
      0x84 => add_a_r8!(self, h),
      0x85 => add_a_r8!(self, l),
      0x86 => add_a_hlp!(self),
      0x87 => add_a_r8!(self, a),

      0x88 => adc_a_r8!(self, b),
      0x89 => adc_a_r8!(self, c),
      0x8a => adc_a_r8!(self, d),
      0x8b => adc_a_r8!(self, e),
      0x8c => adc_a_r8!(self, h),
      0x8d => adc_a_r8!(self, l),
      0x8e => adc_a_hlp!(self),
      0x8f => adc_a_r8!(self, a),

      0x90 => sub_r8!(self, b),
      0x91 => sub_r8!(self, c),
      0x92 => sub_r8!(self, d),
      0x93 => sub_r8!(self, e),
      0x94 => sub_r8!(self, h),
      0x95 => sub_r8!(self, l),
      0x96 => sub_hlp!(self),
      0x97 => sub_r8!(self, a),

      0x98 => sbc_r8!(self, b),
      0x99 => sbc_r8!(self, c),
      0x9a => sbc_r8!(self, d),
      0x9b => sbc_r8!(self, e),
      0x9c => sbc_r8!(self, h),
      0x9d => sbc_r8!(self, l),
      0x9e => sbc_hlp!(self),
      0x9f => sbc_r8!(self, a),

      0xa0 => and_r8!(self, b),
      0xa1 => and_r8!(self, c),
      0xa2 => and_r8!(self, d),
      0xa3 => and_r8!(self, e),
      0xa4 => and_r8!(self, h),
      0xa5 => and_r8!(self, l),
      0xa6 => and_hlp!(self),
      0xa7 => and_r8!(self, a),

      0xa8 => xor_r8!(self, b),
      0xa9 => xor_r8!(self, c),
      0xaa => xor_r8!(self, d),
      0xab => xor_r8!(self, e),
      0xac => xor_r8!(self, h),
      0xad => xor_r8!(self, l),
      0xae => xor_hlp!(self),
      0xaf => xor_r8!(self, a),

      0xb0 => or_r8!(self, b),
      0xb1 => or_r8!(self, c),
      0xb2 => or_r8!(self, d),
      0xb3 => or_r8!(self, e),
      0xb4 => or_r8!(self, h),
      0xb5 => or_r8!(self, l),
      0xb6 => or_hlp!(self),
      0xb7 => or_r8!(self, a),

      0xb8 => cp_r8!(self, b),
      0xb9 => cp_r8!(self, c),
      0xba => cp_r8!(self, d),
      0xbb => cp_r8!(self, e),
      0xbc => cp_r8!(self, h),
      0xbd => cp_r8!(self, l),
      0xbe => cp_hlp!(self),
      0xbf => cp_r8!(self, a),

      0xc0 => ret!(self when z is false),
      0xc1 => pop!(self into bc),
      0xc2 => jp!(self when z is false),
      0xc3 => jp!(self),
      0xc4 => call!(self when z is false),
      0xc5 => push!(self from bc),

      0xc8 => ret!(self when z is true),
      0xc9 => ret!(self),
      0xca => jp!(self when z is true),
      0xcc => call!(self when z is true),
      0xcd => call!(self a16),

      0xd0 => ret!(self when c is false),
      0xd1 => pop!(self into de),
      0xd2 => jp!(self when c is false),
      0xd4 => call!(self when c is false),
      0xd5 => push!(self from de),

      0xd8 => ret!(self when c is true),
      0xda => jp!(self when c is true),
      0xdc => call!(self when c is true),

      0xe1 => pop!(self into hl),
      0xe5 => push!(self from hl),

      0xf1 => pop!(self into af),
      0xf5 => push!(self from af),

      _ => panic!(format!("Unknown opcode: 0x{:>02x}", opcode)),
    }
  }

  fn run_rotate(&mut self) -> usize {
    read_code!(u8 into rotation from self);

    match rotation {
      _ => panic!(format!("Unknown rotation: 0xcb{:>02x}", rotation)),
    }
  }
}
